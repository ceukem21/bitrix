<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$arr['DESTINATION']	= 'form';
$arr['PAGE_LIST'] = './';
$arr['PAGE_ADD'] = './form.php';
$arr['PAGE_DETAIL'] = './detail.php?id=#ID#'; 

$APPLICATION->IncludeComponent(
	"askarasoft:workflow.crud",
	"." . $arr['DESTINATION'],
	$arr,
	false
);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
