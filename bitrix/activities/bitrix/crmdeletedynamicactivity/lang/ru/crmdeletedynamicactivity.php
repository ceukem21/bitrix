<?php

$MESS['CRM_DDA_DYNAMIC_TYPE'] = 'Тип элемента смарт-процесса';
$MESS['CRM_DDA_DYNAMIC_ID'] = 'ID элемента смарт-процесса';

$MESS['CRM_DDA_TYPE_ID_ERROR'] = 'Неверный тип сущности';
$MESS['CRM_DDA_ENTITY_ERROR'] = 'Сущности с выбранным id не существует';
$MESS['CRM_DDA_DELETE_ERROR'] = 'Во время удаления элемента произошла ошибка';