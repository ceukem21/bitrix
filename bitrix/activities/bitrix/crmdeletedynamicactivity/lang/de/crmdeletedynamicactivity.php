<?php
$MESS["CRM_DDA_DELETE_ERROR"] = "Fehler beim Löschen des Elements";
$MESS["CRM_DDA_DYNAMIC_ID"] = "Smartprozess-ID";
$MESS["CRM_DDA_DYNAMIC_TYPE"] = "Smartprozesstyp";
$MESS["CRM_DDA_ENTITY_ERROR"] = "Element mit dieser ID existiert nicht";
$MESS["CRM_DDA_TYPE_ID_ERROR"] = "Ungültiger Elementtyp";
