<?php

$MESS['CRM_UDA_DYNAMIC_TYPE'] = 'Тип элемента смарт-процесса';
$MESS['CRM_UDA_DYNAMIC_ID'] = 'ID элемента смарт-процесса';
$MESS['CRM_UDA_ADD_CONDITION'] = 'Выбрать поле';
$MESS['CRM_UDA_DELETE_CONDITION'] = 'Удалить';

$MESS['CRM_UDA_ENTITY_TYPE_ERROR'] = 'Выбран неверный тип сущности';
$MESS['CRM_UDA_ENTITY_ID_ERROR'] = 'Id сущности должен быть больше 0';
$MESS['CRM_UDA_ENTITY_EXISTENCE_ERROR'] = 'Не существует элемента типа "#TYPE_NAME#" с ID "#ENTITY_ID#"';
