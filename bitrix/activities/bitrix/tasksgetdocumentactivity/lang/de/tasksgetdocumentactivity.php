<?php
$MESS["TASKS_GLDA_ACCESS_DENIED"] = "Nur die Bitrix24 Administratoren können auf die Aktionseigenschaften zugreifen.";
$MESS["TASKS_GLDA_ELEMENT_ID"] = "ID der Aufgabe";
$MESS["TASKS_GLDA_ERROR_ELEMENT_ID"] = "ID der Aufgabe ist erforderlich";
$MESS["TASKS_GLDA_ERROR_EMPTY_DOCUMENT"] = "Dokumentinformationen können nicht bekommen werden";
$MESS["TASKS_GLDA_ERROR_FIELDS"] = "Es wurden keine Dokumentfelder ausgewählt";
$MESS["TASKS_GLDA_FIELDS_LABEL"] = "Felder auswählen";
