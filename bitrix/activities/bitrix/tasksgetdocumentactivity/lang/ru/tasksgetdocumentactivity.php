<?php

$MESS['TASKS_GLDA_ELEMENT_ID'] = 'ID задачи';
$MESS['TASKS_GLDA_FIELDS_LABEL'] = 'Выберите поля';

$MESS['TASKS_GLDA_ERROR_ELEMENT_ID'] = "Не заполнено ID задачи";
$MESS['TASKS_GLDA_ERROR_FIELDS'] = "Не выбраны поля документа";
$MESS['TASKS_GLDA_ERROR_EMPTY_DOCUMENT'] = 'Не удалось получить данные документа';
$MESS['TASKS_GLDA_ACCESS_DENIED'] = 'Настройки действия доступны только администраторам портала.';
