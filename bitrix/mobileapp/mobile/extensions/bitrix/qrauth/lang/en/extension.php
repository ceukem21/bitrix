<?php
$MESS["AUTH_WAIT"] = "Logging you in...";
$MESS["BROWSERS"] = "#1# Google Chrome, Microsoft Edge, Safari, Mozilla Firefox, Opera and other browsers
  ";
$MESS["GET_MORE"] = "Get even more business tools in the full version of Bitrix24.";
$MESS["OPEN_BROWSER"] = "Open your browser #1# and enter this URL in the address bar: [SIZE=13][B][COLOR=#4578E7]#DOMAIN#[/COLOR][/B][/SIZE]";
$MESS["SCAN_QR"] = "Scan the QR code with your mobile camera";
$MESS["WRONG_QR"] = "QR code is incorrect";
