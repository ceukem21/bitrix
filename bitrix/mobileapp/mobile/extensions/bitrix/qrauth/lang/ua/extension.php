<?php
$MESS["AUTH_WAIT"] = "Авторизуємо...";
$MESS["BROWSERS"] = "#1# Google Chrome, Microsoft Edge, Safari,
Mozilla Firefox, Opera та інші
  ";
$MESS["GET_MORE"] = "Отримайте ще більше можливостей для вашого бізнесу
в повній версії Бітрікс24.";
$MESS["OPEN_BROWSER"] = "Відкрийте браузер#1# на комп'ютері
та в рядку наберіть: [SIZE=13][B][COLOR=#4578E7]#DOMAIN#[/COLOR][/B][/SIZE]";
$MESS["SCAN_QR"] = "Наведіть камеру на QR-код,
що з'явився у вікні ";
$MESS["WRONG_QR"] = "Неправильний QR-код";
