<?php
$MESS['OPEN_BROWSER'] = 'Откройте [SIZE=13][B][COLOR=#4578E7]#DOMAIN#[/COLOR][/B][/SIZE] на компьютере';
$MESS['SCAN_QR'] = 'Наведите камеру телефона на [B]QR-код[/B]';
$MESS['GET_MORE'] = 'Получите ещё больше возможностей для вашего бизнеса 
в полной версии Битрикс24.';
$MESS["AUTH_WAIT"] = "Авторизуем...";
$MESS["WRONG_QR"] = "Неверный qr-код";
