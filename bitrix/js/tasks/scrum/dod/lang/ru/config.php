<?php
$MESS["TASKS_SCRUM_DOD_HEADER"] = "Definition of Done";
$MESS["TASKS_SCRUM_ERROR_ALL_TOGGLED"] = "Все пункты должны быть выполнены";
$MESS["TASKS_SCRUM_DOD_LABEL_TYPES"] = "Тип задачи";
$MESS["TASKS_SCRUM_DOD_INFO_TEXT"] = "Для завершения задачи необходимо выполнить все пункты Definition of Done";
$MESS["TASKS_SCRUM_DOD_CONFIRM_TEXT_COMPLETE"] = "Вы хотите завершить задачу или сохранить Definition of Done?";
$MESS["TASKS_SCRUM_DOD_CONFIRM_COMPLETE_BUTTON_TEXT"] = "Завершить";
$MESS["TASKS_SCRUM_DOD_CONFIRM_SAVE_BUTTON_TEXT"] = "Сохранить";
$MESS["TASKS_SCRUM_DOD_CONFIRM_CLOSE_BUTTON_TEXT"] = "Закрыть";
$MESS["TASKS_SCRUM_DOD_LABEL_EMPTY"] = "Необходимо настроить Definition of Done";