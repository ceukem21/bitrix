/**
 * @namespace {BX.UI}
 */
export default class CounterSize
{
	static SMALL = "ui-counter-lg";
	static LARGE = "ui-counter-lg";
	static MEDIUM = "ui-counter-md";
}
