/**
 * @namespace {BX.UI}
 */
export default class CounterColor
{
	static DANGER = "ui-counter-danger";
	static SUCCESS = "ui-counter-success";
	static PRIMARY = "ui-counter-primary";
	static GRAY = "ui-counter-gray";
	static LIGHT = "ui-counter-light";
	static DARK = "ui-counter-dark";
}
