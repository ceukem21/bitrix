<?php
$MESS["IMCONNECTOR_MESSAGE_UNSUPPORTED_INSTAGRAM"] = "Customer sent you a message. However, this message type is temporarily not supported. Please use your Instagram app to read the message.";
