<?php
$MESS["SUPPORT24_QUEUE_NUMBER"] = "You are number #QUEUE_NUMBER# in the queue now";
$MESS["SUPPORT24_QUEUE_NUMBER_REFRESH"] = "Refresh";
$MESS["SUPPORT24_START_DIALOG_F"] = "Conversation started by #USER_FULL_NAME#";
$MESS["SUPPORT24_START_DIALOG_M"] = "Conversation started by #USER_FULL_NAME#";
