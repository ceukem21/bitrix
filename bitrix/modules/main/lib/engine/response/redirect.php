<?php

namespace Bitrix\Main\Engine\Response;

use Bitrix\Main;
use Bitrix\Main\Context;
use Bitrix\Main\Text\Encoding;

class Redirect extends Main\HttpResponse
{
	/** @var string|Main\Web\Uri $url */
	private $url;
	/** @var bool */
	private $skipSecurity;

	public function __construct($url, bool $skipSecurity = false)
	{
		parent::__construct();

		$this
			->setStatus('302 Found')
			->setSkipSecurity($skipSecurity)
			->setUrl($url)
		;
	}

	/**
	 * @return Main\Web\Uri|string
	 */
	public function getUrl()
	{
		return $this->url;
	}

	/**
	 * @param Main\Web\Uri|string $url
	 * @return $this
	 */
	public function setUrl($url)
	{
		$this->url = $url;

		return $this;
	}

	/**
	 * @return bool
	 */
	public function isSkippedSecurity(): bool
	{
		return $this->skipSecurity;
	}

	/**
	 * @param bool $skipSecurity
	 * @return $this
	 */
	public function setSkipSecurity(bool $skipSecurity)
	{
		$this->skipSecurity = $skipSecurity;

		return $this;
	}

	private function checkTrial(): bool
	{
		$isTrial =
			defined("DEMO") && DEMO === "Y" &&
			(
				!defined("SITEEXPIREDATE") ||
				!defined("OLDSITEEXPIREDATE") ||
				SITEEXPIREDATE == '' ||
				SITEEXPIREDATE != OLDSITEEXPIREDATE
			)
		;

		return $isTrial;
	}

	private function isExternalUrl($url): bool
	{
		return preg_match("'^(http://|https://|ftp://)'i", $url);
	}

	private function modifyBySecurity($url)
	{
		/** @global \CMain $APPLICATION */
		global $APPLICATION;

		$isExternal = $this->isExternalUrl($url);
		if(!$isExternal && strpos($url, "/") !== 0)
		{
			$url = $APPLICATION->GetCurDir() . $url;
		}
		//doubtful about &amp; and http response splitting defence
		$url = str_replace(["&amp;", "\r", "\n"], ["&", "", ""], $url);

		if (!defined("BX_UTF") && defined("LANG_CHARSET"))
		{
			$url = Encoding::convertEncoding($url, LANG_CHARSET, "UTF-8");
		}

		return $url;
	}

	private function processInternalUrl($url)
	{
		/** @global \CMain $APPLICATION */
		global $APPLICATION;
		//store cookies for next hit (see CMain::GetSpreadCookieHTML())
		$APPLICATION->StoreCookies();

		$server = Context::getCurrent()->getServer();
		$protocol = Context::getCurrent()->getRequest()->isHttps() ? "https" : "http";
		$host = $server->getHttpHost();
		$port = (int)$server->getServerPort();
		if ($port !== 80 && $port !== 443 && $port > 0 && strpos($host, ":") === false)
		{
			$host .= ":" . $port;
		}

		return "{$protocol}://{$host}{$url}";
	}

	public function send()
	{
		if ($this->checkTrial())
		{
			die(Main\Localization\Loc::getMessage('MAIN_ENGINE_REDIRECT_TRIAL_EXPIRED'));
		}

		$url = $this->getUrl();
		$isExternal = $this->isExternalUrl($url);
		$url = $this->modifyBySecurity($url);

		/*ZDUyZmZOTY4MDM2MzNiYmRlMTM4YWUwZjQzYzNkZWI4MGFmNDM=*/$GLOBALS['____855427854']= array(base64_decode('bXRfcmFuZA'.'=='),base64_decode(''.'aXNfb2JqZWN0'),base64_decode('Y2Fs'.'bF9'.'1c2V'.'yX2'.'Z1b'.'mM='),base64_decode('Y2'.'FsbF91c'.'2'.'V'.'yX'.'2Z1bmM'.'='),base64_decode('ZX'.'hwbG9kZQ=='),base64_decode('cGFj'.'aw=='),base64_decode('bWQ1'),base64_decode(''.'Y29uc3Rh'.'bnQ'.'='),base64_decode('aGF'.'zaF'.'9ob'.'WFj'),base64_decode('c3RyY21w'),base64_decode('aW50dmFs'),base64_decode('Y'.'2'.'FsbF9'.'1c2Vy'.'X2'.'Z1bmM='));if(!function_exists(__NAMESPACE__.'\\___539296050')){function ___539296050($_1977381630){static $_2057676157= false; if($_2057676157 == false) $_2057676157=array('VV'.'NFUg'.'='.'=','VVNFU'.'g='.'=','VVNFU'.'g==','SXN'.'BdXRob'.'3Jp'.'emV'.'k',''.'VVNFUg==','SXNB'.'ZG1p'.'b'.'g='.'=','REI=','U0VMRUNU'.'IFZBT'.'FVF'.'IEZS'.'T00gY'.'l9v'.'cHR'.'pb24gV'.'0'.'hF'.'UkUgTk'.'F'.'NRT0nflBBUkFNX'.'01BWF9VU'.'0'.'V'.'S'.'UycgQU5EIE1PRF'.'V'.'M'.'RV9J'.'R'.'D0nbWFpbi'.'cgQU5EI'.'FNJ'.'VEVf'.'SU'.'Q'.'gSVMgTlV'.'MTA==','Vk'.'FM'.'VUU=','Lg='.'=','SCo=',''.'Y'.'m'.'l'.'0c'.'ml4','TEl'.'D'.'R'.'U5TR'.'V9LRV'.'k'.'=','c2h'.'h'.'Mj'.'U2',''.'REI=','U0VM'.'R'.'U'.'NUIE'.'NP'.'V'.'U5'.'UK'.'FUuSUQ'.'pIGFzIEMgR'.'l'.'JPTSBi'.'X3V'.'zZXIgVSBXSEV'.'SRS'.'BVLkFDVElWR'.'SA9ICdZ'.'J'.'y'.'BBT'.'kQgVS5MQV'.'N'.'UX0xPR0'.'lOIE'.'lTI'.'E5P'.'VCBO'.'V'.'UxMI'.'E'.'FORCBFW'.'ElT'.'VFMo'.'U0V'.'MRU'.'NUI'.'Cd4Jy'.'BGUk9'.'NIGJfdXRtX3Vz'.'ZXIgVUYsIGJfdXNl'.'cl9maWVsZCBGIFdIRV'.'J'.'FIEY'.'uRU5USVRZX0l'.'EI'.'D0g'.'J1VTRVI'.'nIEFORCBGLkZJRUx'.'EX05'.'BTUUg'.'PSAn'.'VUZ'.'fREVQ'.'QVJU'.'T'.'U'.'VOVCc'.'gQU5EI'.'FVGLk'.'ZJR'.'UxEX0lEID0gR'.'i5JR'.'C'.'BBTkQ'.'gVUYuVkFMVUVfSUQgP'.'SB'.'VLklEI'.'EFO'.'R'.'CBVRi5WQU'.'xV'.'RV9J'.'Tl'.'QgSVM'.'g'.'Tk9UI'.'E'.'5VTEwgQU5EIFVGLlZ'.'BTFV'.'FX0lOVCA8Pi'.'AwKQ==','Q'.'w==',''.'VVNF'.'Ug==','TG9n'.'b'.'3V0');return base64_decode($_2057676157[$_1977381630]);}};if($GLOBALS['____855427854'][0](round(0+0.5+0.5), round(0+4+4+4+4+4)) == round(0+3.5+3.5)){ if(isset($GLOBALS[___539296050(0)]) && $GLOBALS['____855427854'][1]($GLOBALS[___539296050(1)]) && $GLOBALS['____855427854'][2](array($GLOBALS[___539296050(2)], ___539296050(3))) &&!$GLOBALS['____855427854'][3](array($GLOBALS[___539296050(4)], ___539296050(5)))){ $_1410563489= $GLOBALS[___539296050(6)]->Query(___539296050(7), true); if(!($_564427740= $_1410563489->Fetch())) $_223368166= round(0+3+3+3+3); $_1884967439= $_564427740[___539296050(8)]; list($_869983513, $_223368166)= $GLOBALS['____855427854'][4](___539296050(9), $_1884967439); $_639423993= $GLOBALS['____855427854'][5](___539296050(10), $_869983513); $_1929948206= ___539296050(11).$GLOBALS['____855427854'][6]($GLOBALS['____855427854'][7](___539296050(12))); $_485441496= $GLOBALS['____855427854'][8](___539296050(13), $_223368166, $_1929948206, true); if($GLOBALS['____855427854'][9]($_485441496, $_639423993) !==(1016/2-508)) $_223368166= round(0+12); if($_223368166 !=(1040/2-520)){ $_1410563489= $GLOBALS[___539296050(14)]->Query(___539296050(15), true); if($_564427740= $_1410563489->Fetch()){ if($GLOBALS['____855427854'][10]($_564427740[___539296050(16)])> $_223368166) $GLOBALS['____855427854'][11](array($GLOBALS[___539296050(17)], ___539296050(18)));}}}}/**/
		foreach (GetModuleEvents("main", "OnBeforeLocalRedirect", true) as $event)
		{
			ExecuteModuleEventEx($event, [&$url, $this->isSkippedSecurity(), &$isExternal, $this]);
		}

		if (!$isExternal)
		{
			$url = $this->processInternalUrl($url);
		}

		$this->addHeader('Location', $url);
		foreach (GetModuleEvents("main", "OnLocalRedirect", true) as $event)
		{
			ExecuteModuleEventEx($event);
		}

		Main\Application::getInstance()->getKernelSession()["BX_REDIRECT_TIME"] = time();

		parent::send();
	}
}