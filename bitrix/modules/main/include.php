<?php
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage main
 * @copyright 2001-2013 Bitrix
 */

use Bitrix\Main\Session\Legacy\HealerEarlySessionStart;

require_once(__DIR__."/bx_root.php");
require_once(__DIR__."/start.php");

$application = \Bitrix\Main\Application::getInstance();
$application->initializeExtendedKernel(array(
	"get" => $_GET,
	"post" => $_POST,
	"files" => $_FILES,
	"cookie" => $_COOKIE,
	"server" => $_SERVER,
	"env" => $_ENV
));

//define global application object
$GLOBALS["APPLICATION"] = new CMain;

if(defined("SITE_ID"))
	define("LANG", SITE_ID);

if(defined("LANG"))
{
	if(defined("ADMIN_SECTION") && ADMIN_SECTION===true)
		$db_lang = CLangAdmin::GetByID(LANG);
	else
		$db_lang = CLang::GetByID(LANG);

	$arLang = $db_lang->Fetch();

	if(!$arLang)
	{
		throw new \Bitrix\Main\SystemException("Incorrect site: ".LANG.".");
	}
}
else
{
	$arLang = $GLOBALS["APPLICATION"]->GetLang();
	define("LANG", $arLang["LID"]);
}

if($arLang["CULTURE_ID"] == '')
{
	throw new \Bitrix\Main\SystemException("Culture not found, or there are no active sites or languages.");
}

$lang = $arLang["LID"];
if (!defined("SITE_ID"))
	define("SITE_ID", $arLang["LID"]);
define("SITE_DIR", ($arLang["DIR"] ?? ''));
define("SITE_SERVER_NAME", ($arLang["SERVER_NAME"] ?? ''));
define("SITE_CHARSET", $arLang["CHARSET"]);
define("FORMAT_DATE", $arLang["FORMAT_DATE"]);
define("FORMAT_DATETIME", $arLang["FORMAT_DATETIME"]);
define("LANG_DIR", ($arLang["DIR"] ?? ''));
define("LANG_CHARSET", $arLang["CHARSET"]);
define("LANG_ADMIN_LID", $arLang["LANGUAGE_ID"]);
define("LANGUAGE_ID", $arLang["LANGUAGE_ID"]);

$culture = \Bitrix\Main\Localization\CultureTable::getByPrimary($arLang["CULTURE_ID"], ["cache" => ["ttl" => CACHED_b_lang]])->fetchObject();

$context = $application->getContext();
$context->setLanguage(LANGUAGE_ID);
$context->setCulture($culture);

$request = $context->getRequest();
if (!$request->isAdminSection())
{
	$context->setSite(SITE_ID);
}

$application->start();

$GLOBALS["APPLICATION"]->reinitPath();

if (!defined("POST_FORM_ACTION_URI"))
{
	define("POST_FORM_ACTION_URI", htmlspecialcharsbx(GetRequestUri()));
}

$GLOBALS["MESS"] = [];
$GLOBALS["ALL_LANG_FILES"] = [];
IncludeModuleLangFile(__DIR__."/tools.php");
IncludeModuleLangFile(__FILE__);

error_reporting(COption::GetOptionInt("main", "error_reporting", E_COMPILE_ERROR|E_ERROR|E_CORE_ERROR|E_PARSE) & ~E_STRICT & ~E_DEPRECATED);

if(!defined("BX_COMP_MANAGED_CACHE") && COption::GetOptionString("main", "component_managed_cache_on", "Y") <> "N")
{
	define("BX_COMP_MANAGED_CACHE", true);
}

// global functions
require_once(__DIR__."/filter_tools.php");

define('BX_AJAX_PARAM_ID', 'bxajaxid');

/*ZDUyZmZNGZmYTM2MGI5NmM1OGM5OWNlNjU1NTBlZjMwOGEwOWY=*/$GLOBALS['_____1600378782']= array(base64_decode('R'.'2V0TW'.'9kd'.'WxlR'.'XZlbnRz'),base64_decode(''.'RXhlY3V'.'0'.'ZU'.'1vZHVsZUV2ZW50RX'.'g='));$GLOBALS['____1374791554']= array(base64_decode(''.'ZGV'.'m'.'aW'.'5l'),base64_decode('c3'.'Ryb'.'GVu'),base64_decode('Y'.'mFzZTY'.'0X2'.'RlY29'.'kZQ'.'='.'='),base64_decode(''.'dW5zZXJ'.'pY'.'WxpemU='),base64_decode('aX'.'NfYX'.'JyYXk='),base64_decode('Y291'.'bnQ='),base64_decode(''.'aW5fY'.'XJyYX'.'k='),base64_decode('c2VyaWF'.'s'.'a'.'Xpl'),base64_decode('YmFzZTY0X'.'2VuY29kZ'.'Q=='),base64_decode(''.'c3'.'Ry'.'b'.'GVu'),base64_decode('YX'.'JyY'.'X'.'lfa2V'.'5X2V4aXN0cw=='),base64_decode('YXJ'.'yYXlf'.'a2'.'V'.'5X2V'.'4aXN0cw=='),base64_decode('bWt0aW'.'1l'),base64_decode('ZG'.'F0ZQ=='),base64_decode('ZGF0Z'.'Q=='),base64_decode('YXJ'.'yYX'.'lfa'.'2V5X2V4'.'aXN0c'.'w=='),base64_decode('c3'.'Ryb'.'GVu'),base64_decode('YXJ'.'yY'.'Xlfa'.'2V5X'.'2'.'V4aXN'.'0cw='.'='),base64_decode('c3Ry'.'b'.'G'.'Vu'),base64_decode('YXJyYXlfa2V5X2V4'.'aXN0cw=='),base64_decode('YXJ'.'yYXlfa2'.'V5X2V'.'4aX'.'N0cw='.'='),base64_decode('bWt0'.'aW1l'),base64_decode('ZGF'.'0ZQ'.'=='),base64_decode('ZG'.'F0Z'.'Q'.'=='),base64_decode('bWV0aG'.'9kX2V4aXN'.'0cw='.'='),base64_decode('Y2'.'FsbF91'.'c2Vy'.'X2'.'Z1b'.'mNfY'.'XJyYXk='),base64_decode('c3Ryb'.'GVu'),base64_decode('YXJyY'.'Xl'.'fa2V'.'5X2V4'.'aXN0c'.'w'.'='.'='),base64_decode('YXJyYXlfa2V5X2'.'V4aXN0cw='.'='),base64_decode('c2VyaW'.'F'.'sa'.'Xpl'),base64_decode(''.'YmFzZT'.'Y0'.'X2VuY2'.'9kZQ=='),base64_decode(''.'c'.'3'.'R'.'ybGVu'),base64_decode('YXJyY'.'Xlfa'.'2V5'.'X'.'2V'.'4aXN'.'0cw='.'='),base64_decode('YXJyYXlf'.'a2V5'.'X2V4a'.'X'.'N0cw'.'='.'='),base64_decode(''.'YXJ'.'y'.'YXl'.'fa2V5'.'X2V'.'4aXN0cw=='),base64_decode('aX'.'Nf'.'YXJyYXk='),base64_decode('YXJ'.'y'.'YXlfa2V5X2V4a'.'X'.'N0cw='.'='),base64_decode('c2V'.'yaWFsaXpl'),base64_decode('Y'.'mFzZTY'.'0X'.'2Vu'.'Y'.'29kZ'.'Q'.'=='),base64_decode('Y'.'XJyYXlfa2V5X2'.'V4'.'aXN0cw=='),base64_decode(''.'YXJ'.'yYXl'.'fa2V5X2V4aXN0'.'c'.'w=='),base64_decode(''.'c2Vya'.'WFsaXpl'),base64_decode(''.'Y'.'mF'.'z'.'ZTY0X2V'.'uY29kZ'.'Q=='),base64_decode('aXNfYXJ'.'yYX'.'k='),base64_decode('aXNfYX'.'JyYXk='),base64_decode('a'.'W5'.'fYX'.'JyYXk='),base64_decode('YXJy'.'YXl'.'fa2V5X2V4aX'.'N'.'0cw=='),base64_decode(''.'aW5fYXJ'.'yYXk='),base64_decode('bWt0aW1l'),base64_decode('ZGF'.'0ZQ=='),base64_decode('ZGF0ZQ='.'='),base64_decode(''.'ZGF0ZQ=='),base64_decode('bW'.'t0aW1'.'l'),base64_decode('ZGF'.'0'.'ZQ=='),base64_decode('ZGF0ZQ=='),base64_decode(''.'a'.'W'.'5fYXJyYXk='),base64_decode('YXJyYXlf'.'a2V5X2V4aXN0c'.'w'.'=='),base64_decode('YXJy'.'YXl'.'fa2V'.'5'.'X2V4aXN0cw='.'='),base64_decode(''.'c2V'.'yaWFs'.'a'.'X'.'pl'),base64_decode(''.'Ym'.'FzZTY'.'0'.'X2VuY29kZQ=='),base64_decode('YXJyY'.'Xlfa2'.'V5'.'X2V4a'.'XN0c'.'w='.'='),base64_decode('aW50dmF'.'s'),base64_decode('dGltZQ=='),base64_decode('YXJyY'.'X'.'lf'.'a'.'2V5X'.'2V4aXN0c'.'w=='),base64_decode(''.'Z'.'mlsZV'.'9leGlzdHM='),base64_decode('c3RyX3J'.'lc'.'Gxh'.'Y2'.'U='),base64_decode('Y2xhc3N'.'fZXhpc3Rz'),base64_decode(''.'ZGVma'.'W'.'5l'));if(!function_exists(__NAMESPACE__.'\\___914304320')){function ___914304320($_343330377){static $_337575768= false; if($_337575768 == false) $_337575768=array('SU5U'.'UkFORVRfRURJVE'.'lPTg'.'='.'=',''.'W'.'Q==',''.'bW'.'Fpbg==','fmN'.'wZl9'.'tYXBfd'.'mF'.'s'.'d'.'WU=','','ZQ==','Zg='.'=','Z'.'Q==',''.'Rg==','W'.'A==','Zg==','bWFpbg==','fmN'.'wZ'.'l9tYXBf'.'dmF'.'sd'.'WU'.'=','UG9ydGFs',''.'Rg==','Z'.'Q='.'=',''.'ZQ='.'=','WA='.'=','Rg==','RA==',''.'RA='.'=','bQ'.'==','ZA==','WQ='.'=','Zg==','Z'.'g==','Zg='.'=','Zg==','UG9ydGFs','Rg='.'=','ZQ==','ZQ==','WA'.'==','Rg==','RA='.'=','RA==',''.'bQ'.'='.'=','Z'.'A='.'=','W'.'Q='.'=',''.'bWFpbg'.'==','T'.'24'.'=',''.'U'.'2V0'.'dGlu'.'Z3N'.'DaGFuZ2'.'U=','Zg'.'='.'=','Zg==','Zg'.'==',''.'Zg='.'=','bWFp'.'bg'.'='.'=','f'.'mNwZl9t'.'YXBfdmFsd'.'WU=','ZQ==','ZQ'.'==','ZQ='.'=','RA==','ZQ==','ZQ==','Z'.'g='.'=',''.'Zg==','Zg==','Z'.'Q='.'=','b'.'WFpbg==','f'.'mNwZl9tY'.'XBf'.'dmFsdWU'.'=','ZQ==','Zg==','Zg==','Zg==','Zg==',''.'bWFpbg==','fmNw'.'Zl9tY'.'XB'.'fdm'.'FsdWU=','ZQ'.'==','Zg==','UG9'.'yd'.'GFs','UG9ydGF'.'s','ZQ==','ZQ='.'=',''.'UG9ydGFs','Rg==','WA==',''.'Rg==','RA==',''.'Z'.'Q==','ZQ==',''.'RA==','b'.'Q==','ZA==','WQ==',''.'Z'.'Q==','WA==','ZQ'.'==','Rg==','ZQ==','RA==','Zg='.'=',''.'ZQ'.'==','R'.'A==','ZQ='.'=','bQ==','ZA==',''.'WQ==','Zg==','Zg==','Zg==','Zg==','Zg==','Zg==','Zg==','Zg==','bWFpb'.'g==','fm'.'Nw'.'Zl'.'9tY'.'X'.'BfdmFsd'.'WU=',''.'ZQ==','Z'.'Q==','U'.'G9y'.'d'.'G'.'Fs',''.'Rg='.'=',''.'WA'.'==','V'.'F'.'lQRQ==','REFURQ==','Rk'.'V'.'BV'.'FVSRVM=','RV'.'hQSVJF'.'RA='.'=','V'.'FlQRQ==',''.'RA'.'==',''.'VFJZX0RBW'.'VN'.'f'.'Q09VTlQ=',''.'R'.'EF'.'URQ='.'=','VFJZ'.'X0RBWVNfQ09VTlQ'.'=','RVhQSVJF'.'RA==','RkVBVFVSRVM=','Z'.'g==',''.'Zg==','RE9'.'DVU1FTl'.'R'.'fUk9P'.'VA==','L2J'.'pd'.'HJpeC9tb2R1bGVzL'.'w==',''.'L'.'2l'.'u'.'c3R'.'hbGw'.'vaW5kZX'.'gucGh'.'w','Lg==','X'.'w==','c2VhcmN'.'o',''.'Tg'.'='.'=','','','QUNUSVZF','WQ==','c29jaWFsbm'.'V0d29ya'.'w'.'==',''.'YW'.'xsb3dfZnJpZ'.'Wxkcw==','WQ==','S'.'UQ'.'=','c29jaWFsbmV0d29yaw==','YW'.'xsb3d'.'fZn'.'JpZWxkcw==','SU'.'Q'.'=',''.'c29ja'.'WFsbm'.'V0d'.'2'.'9yaw==','YWxsb3d'.'f'.'ZnJpZ'.'W'.'xk'.'c'.'w='.'=','T'.'g==','','','QUNUSVZF','W'.'Q==','c2'.'9'.'j'.'a'.'W'.'Fs'.'b'.'mV0d2'.'9yaw'.'==','YWxs'.'b3dfbW'.'l'.'jc'.'m'.'9ibG9nX3VzZX'.'I=','WQ==','SUQ=',''.'c2'.'9ja'.'WFsb'.'mV0d2'.'9ya'.'w'.'==',''.'Y'.'Wxs'.'b'.'3dfbWljcm'.'9ibG9n'.'X'.'3'.'Vz'.'ZXI=','SU'.'Q=','c2'.'9j'.'a'.'W'.'Fs'.'b'.'mV0d29yaw==','YWx'.'sb3'.'dfbWljcm9ibG9'.'nX'.'3VzZXI=','c29'.'jaW'.'FsbmV'.'0d29yaw==','YWx'.'sb'.'3d'.'fbWlj'.'cm9ibG9nX2d'.'y'.'b3Vw','WQ==','SUQ=','c2'.'9'.'j'.'aWFs'.'bmV0d2'.'9yaw==','Y'.'Wxsb3dfb'.'Wljcm'.'9ibG9nX2dyb'.'3'.'Vw','SUQ=','c2'.'9jaWFsbmV0d29ya'.'w==','YWxsb3d'.'fbWljcm9ibG9nX2dyb3V'.'w','T'.'g==','','','QUNU'.'S'.'VZ'.'F','WQ==','c'.'2'.'9jaWFsbm'.'V0d2'.'9y'.'aw==','YWx'.'sb3d'.'fZm'.'ls'.'ZXNf'.'dX'.'Nlcg==','W'.'Q'.'==','SU'.'Q'.'=','c2'.'9jaW'.'FsbmV0d'.'29y'.'aw='.'=','Y'.'Wxsb3'.'dfZml'.'s'.'ZXNfdXNlcg==','SUQ=',''.'c29jaWFsbmV0d29yaw'.'==',''.'YWx'.'s'.'b'.'3dfZml'.'sZXNfdXNlcg==','Tg='.'=','','','Q'.'UNUSVZ'.'F','W'.'Q==',''.'c29jaWF'.'s'.'bmV0d'.'29yaw'.'==','YWxsb3df'.'Ym'.'xvZ191c'.'2Vy','WQ==','SUQ=','c29j'.'a'.'WFsbmV0d2'.'9yaw==','YWxs'.'b3df'.'Y'.'mxvZ'.'191c2'.'V'.'y',''.'SUQ=','c29'.'jaW'.'FsbmV0d29yaw==',''.'YWxsb3'.'dfY'.'mxvZ1'.'91'.'c2Vy',''.'T'.'g==','','',''.'QUNU'.'SVZF','WQ==',''.'c29'.'jaWF'.'sbmV0'.'d29ya'.'w'.'==','Y'.'W'.'xsb3dfcGhv'.'dG9f'.'d'.'XNlcg==','WQ==','S'.'UQ=','c'.'2'.'9jaWFsbmV'.'0d29yaw'.'==',''.'YWx'.'s'.'b3d'.'fcGh'.'vdG9fdXNlcg==','SUQ'.'=','c29'.'jaWFs'.'bm'.'V0d29yaw'.'='.'=','Y'.'Wxs'.'b3dfcGhvdG9'.'fd'.'XN'.'lcg==','Tg='.'=','','',''.'QUNUSVZF','WQ'.'='.'=','c'.'29ja'.'W'.'F'.'sbmV0d29yaw='.'=',''.'YW'.'xsb'.'3'.'dfZm9ydW'.'1f'.'dXNlcg==','WQ==','SUQ=','c'.'29jaWF'.'sbmV0d29ya'.'w==','YWx'.'s'.'b3'.'dfZm9'.'ydW1'.'fdXNl'.'cg==','S'.'UQ=','c2'.'9jaWFsb'.'m'.'V0d29yaw'.'==','YWxsb3d'.'fZm9ydW'.'1fd'.'XNl'.'cg='.'=','T'.'g==','','','QU'.'N'.'USVZF','WQ==','c29ja'.'WFsbmV0d'.'29ya'.'w==','YW'.'xsb'.'3dfdG'.'F'.'za3N'.'fd'.'X'.'N'.'lcg'.'==','WQ='.'=','SU'.'Q'.'=','c29jaWFsbmV'.'0d29yaw==',''.'YWxsb'.'3df'.'dGFza3Nfd'.'XNlcg==','SUQ=','c2'.'9jaWFsbmV0d29yaw='.'=','YWxsb3'.'dfdG'.'Fza3N'.'fdXN'.'lcg==','c'.'2'.'9ja'.'W'.'FsbmV0'.'d'.'29yaw==','Y'.'Wxsb3dfdG'.'Fza3NfZ3JvdXA=',''.'WQ==','SUQ'.'=','c29'.'j'.'a'.'WFsb'.'mV0'.'d2'.'9yaw==',''.'YWxsb3dfdGFza3NfZ3J'.'v'.'d'.'XA=','S'.'U'.'Q=','c'.'29ja'.'WFsbmV0d'.'29'.'yaw==','YWxsb3dfdGFza3NfZ3JvdXA=','d'.'GFz'.'a3M=',''.'Tg'.'='.'=','','','QUNUSVZF',''.'WQ==','c29'.'ja'.'WFsbmV'.'0d29ya'.'w==',''.'YW'.'xsb3df'.'Y2FsZW5k'.'YXJfdXN'.'lcg='.'=','W'.'Q==','SUQ=','c29'.'jaWFsbmV0'.'d29yaw==','YWxsb3dfY'.'2FsZW5kYX'.'Jf'.'dXN'.'lcg==','SU'.'Q=','c'.'29'.'jaWFs'.'b'.'mV0d29'.'ya'.'w='.'=',''.'YWxsb'.'3dfY'.'2F'.'sZW5'.'kY'.'XJfdX'.'Nl'.'cg==',''.'c29ja'.'WFsbmV0'.'d29yaw==',''.'Y'.'Wxsb3'.'df'.'Y'.'2FsZW'.'5kYX'.'JfZ3J'.'vd'.'XA=','WQ='.'=',''.'SU'.'Q'.'=','c29jaWF'.'s'.'bmV0'.'d29y'.'aw='.'=','YWx'.'sb3dfY2Fs'.'Z'.'W'.'5k'.'YXJfZ3JvdXA'.'=','SUQ=','c2'.'9'.'jaWFsbmV0d'.'29ya'.'w==','YWx'.'sb'.'3d'.'fY2FsZ'.'W'.'5'.'kYXJfZ3JvdXA=','Q'.'U'.'N'.'U'.'SVZ'.'F','WQ==',''.'Tg='.'=','ZXh0cmFu'.'Z'.'XQ=','aW'.'J'.'sb'.'2Nr','T25'.'BZn'.'Rl'.'cklCbG9ja0V'.'sZ'.'W1lbnR'.'VcGRhdGU'.'=','a'.'W50'.'cmFuZ'.'XQ=',''.'Q0'.'l'.'u'.'dHJhbm'.'V'.'0RXZ'.'lbnRIYW5kbG'.'Vy'.'cw='.'=','U1BSZ'.'W'.'d'.'pc3'.'RlclVwZGF'.'0'.'ZWRJd'.'GVt','Q0lud'.'HJhbmV0U2h'.'h'.'cmV'.'wb2ludDo6'.'QWdl'.'bnRMaXN0cyg'.'pO'.'w'.'==',''.'aW5'.'0cmFuZ'.'XQ=','Tg==','Q0l'.'udHJhbmV'.'0U2hhcmVw'.'b2l'.'ud'.'Do6QWdlb'.'nRR'.'d'.'W'.'V1'.'ZSgpOw==','aW'.'5'.'0cmFuZX'.'Q'.'=','Tg==','Q'.'0'.'lud'.'HJh'.'bmV'.'0U2h'.'hc'.'mVwb2ludDo6'.'Q'.'WdlbnR'.'Vc'.'G'.'Rhd'.'G'.'Uo'.'KTs=','aW'.'50cmFu'.'ZXQ=','Tg==','aWJsb2Nr','T25BZnRlckl'.'CbG9j'.'a0'.'VsZW1lbnR'.'BZGQ=','aW50cmFuZ'.'X'.'Q=',''.'Q0lu'.'dHJh'.'b'.'m'.'V0RXZlbnRI'.'YW'.'5kbGVycw'.'='.'=','U1BSZWdpc'.'3'.'Rlcl'.'Vw'.'Z'.'GF0ZW'.'R'.'J'.'dGVt',''.'aWJ'.'sb2Nr','T25'.'BZ'.'nRl'.'cklCb'.'G9ja0Vs'.'ZW1lbnRVcGRhdGU=','aW50c'.'mFu'.'ZXQ=','Q'.'0ludHJhb'.'mV0RXZl'.'bnRIYW5'.'kbGVycw==','U1BSZWdpc3RlclVwZGF0Z'.'WR'.'J'.'dG'.'Vt','Q0'.'ludHJhbmV0U2hh'.'cmVw'.'b2lu'.'dD'.'o6QW'.'dl'.'bnRMa'.'XN0c'.'ygpOw==','aW5'.'0cmFuZXQ'.'=','Q0l'.'udH'.'Jhbm'.'V'.'0U2hhcmV'.'wb2ludDo6QWdlbn'.'RR'.'dWV1'.'ZSg'.'p'.'Ow==',''.'aW50c'.'mF'.'u'.'ZXQ=','Q0ludHJhbm'.'V0U2'.'hhcm'.'Vwb2ludD'.'o6QWdlbnRVcGRh'.'dGU'.'oK'.'Ts=','aW5'.'0cmF'.'uZ'.'X'.'Q'.'=','Y3Jt','bWFp'.'bg==',''.'T25CZ'.'WZv'.'c'.'mVQc'.'m'.'9sb2'.'c=','bWFpbg==','Q1d'.'pe'.'mFyZ'.'FNv'.'bF'.'Bh'.'bmVs'.'SW'.'5'.'0cmFuZX'.'Q=','U2'.'hvd1BhbmVs','L21vZHVsZXMvaW5'.'0cm'.'FuZXQvcGFuZWxfY'.'nV0d'.'G'.'9'.'uLnBo'.'cA==','RU5'.'DT0RF','WQ==');return base64_decode($_337575768[$_343330377]);}};$GLOBALS['____1374791554'][0](___914304320(0), ___914304320(1));class CBXFeatures{ private static $_74579467= 30; private static $_858332019= array( "Portal" => array( "CompanyCalendar", "CompanyPhoto", "CompanyVideo", "CompanyCareer", "StaffChanges", "StaffAbsence", "CommonDocuments", "MeetingRoomBookingSystem", "Wiki", "Learning", "Vote", "WebLink", "Subscribe", "Friends", "PersonalFiles", "PersonalBlog", "PersonalPhoto", "PersonalForum", "Blog", "Forum", "Gallery", "Board", "MicroBlog", "WebMessenger",), "Communications" => array( "Tasks", "Calendar", "Workgroups", "Jabber", "VideoConference", "Extranet", "SMTP", "Requests", "DAV", "intranet_sharepoint", "timeman", "Idea", "Meeting", "EventList", "Salary", "XDImport",), "Enterprise" => array( "BizProc", "Lists", "Support", "Analytics", "crm", "Controller", "LdapUnlimitedUsers",), "Holding" => array( "Cluster", "MultiSites",),); private static $_1947499207= false; private static $_391159381= false; private static function __748653124(){ if(self::$_1947499207 == false){ self::$_1947499207= array(); foreach(self::$_858332019 as $_1873406354 => $_1036942033){ foreach($_1036942033 as $_1672494621) self::$_1947499207[$_1672494621]= $_1873406354;}} if(self::$_391159381 == false){ self::$_391159381= array(); $_976076183= COption::GetOptionString(___914304320(2), ___914304320(3), ___914304320(4)); if($GLOBALS['____1374791554'][1]($_976076183)>(860-2*430)){ $_976076183= $GLOBALS['____1374791554'][2]($_976076183); self::$_391159381= $GLOBALS['____1374791554'][3]($_976076183); if(!$GLOBALS['____1374791554'][4](self::$_391159381)) self::$_391159381= array();} if($GLOBALS['____1374791554'][5](self::$_391159381) <=(1132/2-566)) self::$_391159381= array(___914304320(5) => array(), ___914304320(6) => array());}} public static function InitiateEditionsSettings($_596323682){ self::__748653124(); $_1717439960= array(); foreach(self::$_858332019 as $_1873406354 => $_1036942033){ $_1552008308= $GLOBALS['____1374791554'][6]($_1873406354, $_596323682); self::$_391159381[___914304320(7)][$_1873406354]=($_1552008308? array(___914304320(8)): array(___914304320(9))); foreach($_1036942033 as $_1672494621){ self::$_391159381[___914304320(10)][$_1672494621]= $_1552008308; if(!$_1552008308) $_1717439960[]= array($_1672494621, false);}} $_1030412191= $GLOBALS['____1374791554'][7](self::$_391159381); $_1030412191= $GLOBALS['____1374791554'][8]($_1030412191); COption::SetOptionString(___914304320(11), ___914304320(12), $_1030412191); foreach($_1717439960 as $_365805750) self::__2027850651($_365805750[(1040/2-520)], $_365805750[round(0+0.2+0.2+0.2+0.2+0.2)]);} public static function IsFeatureEnabled($_1672494621){ if($GLOBALS['____1374791554'][9]($_1672494621) <= 0) return true; self::__748653124(); if(!$GLOBALS['____1374791554'][10]($_1672494621, self::$_1947499207)) return true; if(self::$_1947499207[$_1672494621] == ___914304320(13)) $_1688875390= array(___914304320(14)); elseif($GLOBALS['____1374791554'][11](self::$_1947499207[$_1672494621], self::$_391159381[___914304320(15)])) $_1688875390= self::$_391159381[___914304320(16)][self::$_1947499207[$_1672494621]]; else $_1688875390= array(___914304320(17)); if($_1688875390[min(20,0,6.6666666666667)] != ___914304320(18) && $_1688875390[(1124/2-562)] != ___914304320(19)){ return false;} elseif($_1688875390[(1068/2-534)] == ___914304320(20)){ if($_1688875390[round(0+0.2+0.2+0.2+0.2+0.2)]< $GLOBALS['____1374791554'][12]((1072/2-536),(1076/2-538),(143*2-286), Date(___914304320(21)), $GLOBALS['____1374791554'][13](___914304320(22))- self::$_74579467, $GLOBALS['____1374791554'][14](___914304320(23)))){ if(!isset($_1688875390[round(0+0.5+0.5+0.5+0.5)]) ||!$_1688875390[round(0+0.5+0.5+0.5+0.5)]) self::__252874563(self::$_1947499207[$_1672494621]); return false;}} return!$GLOBALS['____1374791554'][15]($_1672494621, self::$_391159381[___914304320(24)]) || self::$_391159381[___914304320(25)][$_1672494621];} public static function IsFeatureInstalled($_1672494621){ if($GLOBALS['____1374791554'][16]($_1672494621) <= 0) return true; self::__748653124(); return($GLOBALS['____1374791554'][17]($_1672494621, self::$_391159381[___914304320(26)]) && self::$_391159381[___914304320(27)][$_1672494621]);} public static function IsFeatureEditable($_1672494621){ if($GLOBALS['____1374791554'][18]($_1672494621) <= 0) return true; self::__748653124(); if(!$GLOBALS['____1374791554'][19]($_1672494621, self::$_1947499207)) return true; if(self::$_1947499207[$_1672494621] == ___914304320(28)) $_1688875390= array(___914304320(29)); elseif($GLOBALS['____1374791554'][20](self::$_1947499207[$_1672494621], self::$_391159381[___914304320(30)])) $_1688875390= self::$_391159381[___914304320(31)][self::$_1947499207[$_1672494621]]; else $_1688875390= array(___914304320(32)); if($_1688875390[(159*2-318)] != ___914304320(33) && $_1688875390[(136*2-272)] != ___914304320(34)){ return false;} elseif($_1688875390[min(154,0,51.333333333333)] == ___914304320(35)){ if($_1688875390[round(0+0.33333333333333+0.33333333333333+0.33333333333333)]< $GLOBALS['____1374791554'][21](min(222,0,74),(1192/2-596),(200*2-400), Date(___914304320(36)), $GLOBALS['____1374791554'][22](___914304320(37))- self::$_74579467, $GLOBALS['____1374791554'][23](___914304320(38)))){ if(!isset($_1688875390[round(0+1+1)]) ||!$_1688875390[round(0+0.4+0.4+0.4+0.4+0.4)]) self::__252874563(self::$_1947499207[$_1672494621]); return false;}} return true;} private static function __2027850651($_1672494621, $_525739447){ if($GLOBALS['____1374791554'][24]("CBXFeatures", "On".$_1672494621."SettingsChange")) $GLOBALS['____1374791554'][25](array("CBXFeatures", "On".$_1672494621."SettingsChange"), array($_1672494621, $_525739447)); $_1784534743= $GLOBALS['_____1600378782'][0](___914304320(39), ___914304320(40).$_1672494621.___914304320(41)); while($_616904319= $_1784534743->Fetch()) $GLOBALS['_____1600378782'][1]($_616904319, array($_1672494621, $_525739447));} public static function SetFeatureEnabled($_1672494621, $_525739447= true, $_624813797= true){ if($GLOBALS['____1374791554'][26]($_1672494621) <= 0) return; if(!self::IsFeatureEditable($_1672494621)) $_525739447= false; $_525739447=($_525739447? true: false); self::__748653124(); $_36720168=(!$GLOBALS['____1374791554'][27]($_1672494621, self::$_391159381[___914304320(42)]) && $_525739447 || $GLOBALS['____1374791554'][28]($_1672494621, self::$_391159381[___914304320(43)]) && $_525739447 != self::$_391159381[___914304320(44)][$_1672494621]); self::$_391159381[___914304320(45)][$_1672494621]= $_525739447; $_1030412191= $GLOBALS['____1374791554'][29](self::$_391159381); $_1030412191= $GLOBALS['____1374791554'][30]($_1030412191); COption::SetOptionString(___914304320(46), ___914304320(47), $_1030412191); if($_36720168 && $_624813797) self::__2027850651($_1672494621, $_525739447);} private static function __252874563($_1873406354){ if($GLOBALS['____1374791554'][31]($_1873406354) <= 0 || $_1873406354 == "Portal") return; self::__748653124(); if(!$GLOBALS['____1374791554'][32]($_1873406354, self::$_391159381[___914304320(48)]) || $GLOBALS['____1374791554'][33]($_1873406354, self::$_391159381[___914304320(49)]) && self::$_391159381[___914304320(50)][$_1873406354][(226*2-452)] != ___914304320(51)) return; if(isset(self::$_391159381[___914304320(52)][$_1873406354][round(0+2)]) && self::$_391159381[___914304320(53)][$_1873406354][round(0+2)]) return; $_1717439960= array(); if($GLOBALS['____1374791554'][34]($_1873406354, self::$_858332019) && $GLOBALS['____1374791554'][35](self::$_858332019[$_1873406354])){ foreach(self::$_858332019[$_1873406354] as $_1672494621){ if($GLOBALS['____1374791554'][36]($_1672494621, self::$_391159381[___914304320(54)]) && self::$_391159381[___914304320(55)][$_1672494621]){ self::$_391159381[___914304320(56)][$_1672494621]= false; $_1717439960[]= array($_1672494621, false);}} self::$_391159381[___914304320(57)][$_1873406354][round(0+0.5+0.5+0.5+0.5)]= true;} $_1030412191= $GLOBALS['____1374791554'][37](self::$_391159381); $_1030412191= $GLOBALS['____1374791554'][38]($_1030412191); COption::SetOptionString(___914304320(58), ___914304320(59), $_1030412191); foreach($_1717439960 as $_365805750) self::__2027850651($_365805750[(226*2-452)], $_365805750[round(0+0.33333333333333+0.33333333333333+0.33333333333333)]);} public static function ModifyFeaturesSettings($_596323682, $_1036942033){ self::__748653124(); foreach($_596323682 as $_1873406354 => $_1629305101) self::$_391159381[___914304320(60)][$_1873406354]= $_1629305101; $_1717439960= array(); foreach($_1036942033 as $_1672494621 => $_525739447){ if(!$GLOBALS['____1374791554'][39]($_1672494621, self::$_391159381[___914304320(61)]) && $_525739447 || $GLOBALS['____1374791554'][40]($_1672494621, self::$_391159381[___914304320(62)]) && $_525739447 != self::$_391159381[___914304320(63)][$_1672494621]) $_1717439960[]= array($_1672494621, $_525739447); self::$_391159381[___914304320(64)][$_1672494621]= $_525739447;} $_1030412191= $GLOBALS['____1374791554'][41](self::$_391159381); $_1030412191= $GLOBALS['____1374791554'][42]($_1030412191); COption::SetOptionString(___914304320(65), ___914304320(66), $_1030412191); self::$_391159381= false; foreach($_1717439960 as $_365805750) self::__2027850651($_365805750[(864-2*432)], $_365805750[round(0+0.5+0.5)]);} public static function SaveFeaturesSettings($_922130435, $_906149124){ self::__748653124(); $_1754157392= array(___914304320(67) => array(), ___914304320(68) => array()); if(!$GLOBALS['____1374791554'][43]($_922130435)) $_922130435= array(); if(!$GLOBALS['____1374791554'][44]($_906149124)) $_906149124= array(); if(!$GLOBALS['____1374791554'][45](___914304320(69), $_922130435)) $_922130435[]= ___914304320(70); foreach(self::$_858332019 as $_1873406354 => $_1036942033){ if($GLOBALS['____1374791554'][46]($_1873406354, self::$_391159381[___914304320(71)])) $_1267808185= self::$_391159381[___914304320(72)][$_1873406354]; else $_1267808185=($_1873406354 == ___914304320(73))? array(___914304320(74)): array(___914304320(75)); if($_1267808185[(185*2-370)] == ___914304320(76) || $_1267808185[(780-2*390)] == ___914304320(77)){ $_1754157392[___914304320(78)][$_1873406354]= $_1267808185;} else{ if($GLOBALS['____1374791554'][47]($_1873406354, $_922130435)) $_1754157392[___914304320(79)][$_1873406354]= array(___914304320(80), $GLOBALS['____1374791554'][48]((147*2-294), min(234,0,78), min(184,0,61.333333333333), $GLOBALS['____1374791554'][49](___914304320(81)), $GLOBALS['____1374791554'][50](___914304320(82)), $GLOBALS['____1374791554'][51](___914304320(83)))); else $_1754157392[___914304320(84)][$_1873406354]= array(___914304320(85));}} $_1717439960= array(); foreach(self::$_1947499207 as $_1672494621 => $_1873406354){ if($_1754157392[___914304320(86)][$_1873406354][min(188,0,62.666666666667)] != ___914304320(87) && $_1754157392[___914304320(88)][$_1873406354][(167*2-334)] != ___914304320(89)){ $_1754157392[___914304320(90)][$_1672494621]= false;} else{ if($_1754157392[___914304320(91)][$_1873406354][min(168,0,56)] == ___914304320(92) && $_1754157392[___914304320(93)][$_1873406354][round(0+1)]< $GLOBALS['____1374791554'][52]((940-2*470),(1260/2-630), min(12,0,4), Date(___914304320(94)), $GLOBALS['____1374791554'][53](___914304320(95))- self::$_74579467, $GLOBALS['____1374791554'][54](___914304320(96)))) $_1754157392[___914304320(97)][$_1672494621]= false; else $_1754157392[___914304320(98)][$_1672494621]= $GLOBALS['____1374791554'][55]($_1672494621, $_906149124); if(!$GLOBALS['____1374791554'][56]($_1672494621, self::$_391159381[___914304320(99)]) && $_1754157392[___914304320(100)][$_1672494621] || $GLOBALS['____1374791554'][57]($_1672494621, self::$_391159381[___914304320(101)]) && $_1754157392[___914304320(102)][$_1672494621] != self::$_391159381[___914304320(103)][$_1672494621]) $_1717439960[]= array($_1672494621, $_1754157392[___914304320(104)][$_1672494621]);}} $_1030412191= $GLOBALS['____1374791554'][58]($_1754157392); $_1030412191= $GLOBALS['____1374791554'][59]($_1030412191); COption::SetOptionString(___914304320(105), ___914304320(106), $_1030412191); self::$_391159381= false; foreach($_1717439960 as $_365805750) self::__2027850651($_365805750[(804-2*402)], $_365805750[round(0+0.33333333333333+0.33333333333333+0.33333333333333)]);} public static function GetFeaturesList(){ self::__748653124(); $_1468718037= array(); foreach(self::$_858332019 as $_1873406354 => $_1036942033){ if($GLOBALS['____1374791554'][60]($_1873406354, self::$_391159381[___914304320(107)])) $_1267808185= self::$_391159381[___914304320(108)][$_1873406354]; else $_1267808185=($_1873406354 == ___914304320(109))? array(___914304320(110)): array(___914304320(111)); $_1468718037[$_1873406354]= array( ___914304320(112) => $_1267808185[(902-2*451)], ___914304320(113) => $_1267808185[round(0+0.25+0.25+0.25+0.25)], ___914304320(114) => array(),); $_1468718037[$_1873406354][___914304320(115)]= false; if($_1468718037[$_1873406354][___914304320(116)] == ___914304320(117)){ $_1468718037[$_1873406354][___914304320(118)]= $GLOBALS['____1374791554'][61](($GLOBALS['____1374791554'][62]()- $_1468718037[$_1873406354][___914304320(119)])/ round(0+21600+21600+21600+21600)); if($_1468718037[$_1873406354][___914304320(120)]> self::$_74579467) $_1468718037[$_1873406354][___914304320(121)]= true;} foreach($_1036942033 as $_1672494621) $_1468718037[$_1873406354][___914304320(122)][$_1672494621]=(!$GLOBALS['____1374791554'][63]($_1672494621, self::$_391159381[___914304320(123)]) || self::$_391159381[___914304320(124)][$_1672494621]);} return $_1468718037;} private static function __1717071974($_1493426407, $_1943902402){ if(IsModuleInstalled($_1493426407) == $_1943902402) return true; $_652246376= $_SERVER[___914304320(125)].___914304320(126).$_1493426407.___914304320(127); if(!$GLOBALS['____1374791554'][64]($_652246376)) return false; include_once($_652246376); $_592605120= $GLOBALS['____1374791554'][65](___914304320(128), ___914304320(129), $_1493426407); if(!$GLOBALS['____1374791554'][66]($_592605120)) return false; $_1001223874= new $_592605120; if($_1943902402){ if(!$_1001223874->InstallDB()) return false; $_1001223874->InstallEvents(); if(!$_1001223874->InstallFiles()) return false;} else{ if(CModule::IncludeModule(___914304320(130))) CSearch::DeleteIndex($_1493426407); UnRegisterModule($_1493426407);} return true;} protected static function OnRequestsSettingsChange($_1672494621, $_525739447){ self::__1717071974("form", $_525739447);} protected static function OnLearningSettingsChange($_1672494621, $_525739447){ self::__1717071974("learning", $_525739447);} protected static function OnJabberSettingsChange($_1672494621, $_525739447){ self::__1717071974("xmpp", $_525739447);} protected static function OnVideoConferenceSettingsChange($_1672494621, $_525739447){ self::__1717071974("video", $_525739447);} protected static function OnBizProcSettingsChange($_1672494621, $_525739447){ self::__1717071974("bizprocdesigner", $_525739447);} protected static function OnListsSettingsChange($_1672494621, $_525739447){ self::__1717071974("lists", $_525739447);} protected static function OnWikiSettingsChange($_1672494621, $_525739447){ self::__1717071974("wiki", $_525739447);} protected static function OnSupportSettingsChange($_1672494621, $_525739447){ self::__1717071974("support", $_525739447);} protected static function OnControllerSettingsChange($_1672494621, $_525739447){ self::__1717071974("controller", $_525739447);} protected static function OnAnalyticsSettingsChange($_1672494621, $_525739447){ self::__1717071974("statistic", $_525739447);} protected static function OnVoteSettingsChange($_1672494621, $_525739447){ self::__1717071974("vote", $_525739447);} protected static function OnFriendsSettingsChange($_1672494621, $_525739447){ if($_525739447) $_1702625558= "Y"; else $_1702625558= ___914304320(131); $_2032491847= CSite::GetList(($_1552008308= ___914304320(132)),($_503139311= ___914304320(133)), array(___914304320(134) => ___914304320(135))); while($_1250380302= $_2032491847->Fetch()){ if(COption::GetOptionString(___914304320(136), ___914304320(137), ___914304320(138), $_1250380302[___914304320(139)]) != $_1702625558){ COption::SetOptionString(___914304320(140), ___914304320(141), $_1702625558, false, $_1250380302[___914304320(142)]); COption::SetOptionString(___914304320(143), ___914304320(144), $_1702625558);}}} protected static function OnMicroBlogSettingsChange($_1672494621, $_525739447){ if($_525739447) $_1702625558= "Y"; else $_1702625558= ___914304320(145); $_2032491847= CSite::GetList(($_1552008308= ___914304320(146)),($_503139311= ___914304320(147)), array(___914304320(148) => ___914304320(149))); while($_1250380302= $_2032491847->Fetch()){ if(COption::GetOptionString(___914304320(150), ___914304320(151), ___914304320(152), $_1250380302[___914304320(153)]) != $_1702625558){ COption::SetOptionString(___914304320(154), ___914304320(155), $_1702625558, false, $_1250380302[___914304320(156)]); COption::SetOptionString(___914304320(157), ___914304320(158), $_1702625558);} if(COption::GetOptionString(___914304320(159), ___914304320(160), ___914304320(161), $_1250380302[___914304320(162)]) != $_1702625558){ COption::SetOptionString(___914304320(163), ___914304320(164), $_1702625558, false, $_1250380302[___914304320(165)]); COption::SetOptionString(___914304320(166), ___914304320(167), $_1702625558);}}} protected static function OnPersonalFilesSettingsChange($_1672494621, $_525739447){ if($_525739447) $_1702625558= "Y"; else $_1702625558= ___914304320(168); $_2032491847= CSite::GetList(($_1552008308= ___914304320(169)),($_503139311= ___914304320(170)), array(___914304320(171) => ___914304320(172))); while($_1250380302= $_2032491847->Fetch()){ if(COption::GetOptionString(___914304320(173), ___914304320(174), ___914304320(175), $_1250380302[___914304320(176)]) != $_1702625558){ COption::SetOptionString(___914304320(177), ___914304320(178), $_1702625558, false, $_1250380302[___914304320(179)]); COption::SetOptionString(___914304320(180), ___914304320(181), $_1702625558);}}} protected static function OnPersonalBlogSettingsChange($_1672494621, $_525739447){ if($_525739447) $_1702625558= "Y"; else $_1702625558= ___914304320(182); $_2032491847= CSite::GetList(($_1552008308= ___914304320(183)),($_503139311= ___914304320(184)), array(___914304320(185) => ___914304320(186))); while($_1250380302= $_2032491847->Fetch()){ if(COption::GetOptionString(___914304320(187), ___914304320(188), ___914304320(189), $_1250380302[___914304320(190)]) != $_1702625558){ COption::SetOptionString(___914304320(191), ___914304320(192), $_1702625558, false, $_1250380302[___914304320(193)]); COption::SetOptionString(___914304320(194), ___914304320(195), $_1702625558);}}} protected static function OnPersonalPhotoSettingsChange($_1672494621, $_525739447){ if($_525739447) $_1702625558= "Y"; else $_1702625558= ___914304320(196); $_2032491847= CSite::GetList(($_1552008308= ___914304320(197)),($_503139311= ___914304320(198)), array(___914304320(199) => ___914304320(200))); while($_1250380302= $_2032491847->Fetch()){ if(COption::GetOptionString(___914304320(201), ___914304320(202), ___914304320(203), $_1250380302[___914304320(204)]) != $_1702625558){ COption::SetOptionString(___914304320(205), ___914304320(206), $_1702625558, false, $_1250380302[___914304320(207)]); COption::SetOptionString(___914304320(208), ___914304320(209), $_1702625558);}}} protected static function OnPersonalForumSettingsChange($_1672494621, $_525739447){ if($_525739447) $_1702625558= "Y"; else $_1702625558= ___914304320(210); $_2032491847= CSite::GetList(($_1552008308= ___914304320(211)),($_503139311= ___914304320(212)), array(___914304320(213) => ___914304320(214))); while($_1250380302= $_2032491847->Fetch()){ if(COption::GetOptionString(___914304320(215), ___914304320(216), ___914304320(217), $_1250380302[___914304320(218)]) != $_1702625558){ COption::SetOptionString(___914304320(219), ___914304320(220), $_1702625558, false, $_1250380302[___914304320(221)]); COption::SetOptionString(___914304320(222), ___914304320(223), $_1702625558);}}} protected static function OnTasksSettingsChange($_1672494621, $_525739447){ if($_525739447) $_1702625558= "Y"; else $_1702625558= ___914304320(224); $_2032491847= CSite::GetList(($_1552008308= ___914304320(225)),($_503139311= ___914304320(226)), array(___914304320(227) => ___914304320(228))); while($_1250380302= $_2032491847->Fetch()){ if(COption::GetOptionString(___914304320(229), ___914304320(230), ___914304320(231), $_1250380302[___914304320(232)]) != $_1702625558){ COption::SetOptionString(___914304320(233), ___914304320(234), $_1702625558, false, $_1250380302[___914304320(235)]); COption::SetOptionString(___914304320(236), ___914304320(237), $_1702625558);} if(COption::GetOptionString(___914304320(238), ___914304320(239), ___914304320(240), $_1250380302[___914304320(241)]) != $_1702625558){ COption::SetOptionString(___914304320(242), ___914304320(243), $_1702625558, false, $_1250380302[___914304320(244)]); COption::SetOptionString(___914304320(245), ___914304320(246), $_1702625558);}} self::__1717071974(___914304320(247), $_525739447);} protected static function OnCalendarSettingsChange($_1672494621, $_525739447){ if($_525739447) $_1702625558= "Y"; else $_1702625558= ___914304320(248); $_2032491847= CSite::GetList(($_1552008308= ___914304320(249)),($_503139311= ___914304320(250)), array(___914304320(251) => ___914304320(252))); while($_1250380302= $_2032491847->Fetch()){ if(COption::GetOptionString(___914304320(253), ___914304320(254), ___914304320(255), $_1250380302[___914304320(256)]) != $_1702625558){ COption::SetOptionString(___914304320(257), ___914304320(258), $_1702625558, false, $_1250380302[___914304320(259)]); COption::SetOptionString(___914304320(260), ___914304320(261), $_1702625558);} if(COption::GetOptionString(___914304320(262), ___914304320(263), ___914304320(264), $_1250380302[___914304320(265)]) != $_1702625558){ COption::SetOptionString(___914304320(266), ___914304320(267), $_1702625558, false, $_1250380302[___914304320(268)]); COption::SetOptionString(___914304320(269), ___914304320(270), $_1702625558);}}} protected static function OnSMTPSettingsChange($_1672494621, $_525739447){ self::__1717071974("mail", $_525739447);} protected static function OnExtranetSettingsChange($_1672494621, $_525739447){ $_1483386738= COption::GetOptionString("extranet", "extranet_site", ""); if($_1483386738){ $_1493288258= new CSite; $_1493288258->Update($_1483386738, array(___914304320(271) =>($_525739447? ___914304320(272): ___914304320(273))));} self::__1717071974(___914304320(274), $_525739447);} protected static function OnDAVSettingsChange($_1672494621, $_525739447){ self::__1717071974("dav", $_525739447);} protected static function OntimemanSettingsChange($_1672494621, $_525739447){ self::__1717071974("timeman", $_525739447);} protected static function Onintranet_sharepointSettingsChange($_1672494621, $_525739447){ if($_525739447){ RegisterModuleDependences("iblock", "OnAfterIBlockElementAdd", "intranet", "CIntranetEventHandlers", "SPRegisterUpdatedItem"); RegisterModuleDependences(___914304320(275), ___914304320(276), ___914304320(277), ___914304320(278), ___914304320(279)); CAgent::AddAgent(___914304320(280), ___914304320(281), ___914304320(282), round(0+250+250)); CAgent::AddAgent(___914304320(283), ___914304320(284), ___914304320(285), round(0+150+150)); CAgent::AddAgent(___914304320(286), ___914304320(287), ___914304320(288), round(0+1800+1800));} else{ UnRegisterModuleDependences(___914304320(289), ___914304320(290), ___914304320(291), ___914304320(292), ___914304320(293)); UnRegisterModuleDependences(___914304320(294), ___914304320(295), ___914304320(296), ___914304320(297), ___914304320(298)); CAgent::RemoveAgent(___914304320(299), ___914304320(300)); CAgent::RemoveAgent(___914304320(301), ___914304320(302)); CAgent::RemoveAgent(___914304320(303), ___914304320(304));}} protected static function OncrmSettingsChange($_1672494621, $_525739447){ if($_525739447) COption::SetOptionString("crm", "form_features", "Y"); self::__1717071974(___914304320(305), $_525739447);} protected static function OnClusterSettingsChange($_1672494621, $_525739447){ self::__1717071974("cluster", $_525739447);} protected static function OnMultiSitesSettingsChange($_1672494621, $_525739447){ if($_525739447) RegisterModuleDependences("main", "OnBeforeProlog", "main", "CWizardSolPanelIntranet", "ShowPanel", 100, "/modules/intranet/panel_button.php"); else UnRegisterModuleDependences(___914304320(306), ___914304320(307), ___914304320(308), ___914304320(309), ___914304320(310), ___914304320(311));} protected static function OnIdeaSettingsChange($_1672494621, $_525739447){ self::__1717071974("idea", $_525739447);} protected static function OnMeetingSettingsChange($_1672494621, $_525739447){ self::__1717071974("meeting", $_525739447);} protected static function OnXDImportSettingsChange($_1672494621, $_525739447){ self::__1717071974("xdimport", $_525739447);}} $GLOBALS['____1374791554'][67](___914304320(312), ___914304320(313));/**/			//Do not remove this

//component 2.0 template engines
$GLOBALS["arCustomTemplateEngines"] = [];

require_once(__DIR__."/autoload.php");
require_once(__DIR__."/classes/general/menu.php");
require_once(__DIR__."/classes/mysql/usertype.php");

if(file_exists(($_fname = __DIR__."/classes/general/update_db_updater.php")))
{
	$US_HOST_PROCESS_MAIN = False;
	include($_fname);
}

if(file_exists(($_fname = $_SERVER["DOCUMENT_ROOT"]."/bitrix/init.php")))
	include_once($_fname);

if(($_fname = getLocalPath("php_interface/init.php", BX_PERSONAL_ROOT)) !== false)
	include_once($_SERVER["DOCUMENT_ROOT"].$_fname);

if(($_fname = getLocalPath("php_interface/".SITE_ID."/init.php", BX_PERSONAL_ROOT)) !== false)
	include_once($_SERVER["DOCUMENT_ROOT"].$_fname);

if(!defined("BX_FILE_PERMISSIONS"))
	define("BX_FILE_PERMISSIONS", 0644);
if(!defined("BX_DIR_PERMISSIONS"))
	define("BX_DIR_PERMISSIONS", 0755);

//global var, is used somewhere
$GLOBALS["sDocPath"] = $GLOBALS["APPLICATION"]->GetCurPage();

if((!(defined("STATISTIC_ONLY") && STATISTIC_ONLY && mb_substr($GLOBALS["APPLICATION"]->GetCurPage(), 0, mb_strlen(BX_ROOT."/admin/")) != BX_ROOT."/admin/")) && COption::GetOptionString("main", "include_charset", "Y")=="Y" && LANG_CHARSET <> '')
	header("Content-Type: text/html; charset=".LANG_CHARSET);

if(COption::GetOptionString("main", "set_p3p_header", "Y")=="Y")
	header("P3P: policyref=\"/bitrix/p3p.xml\", CP=\"NON DSP COR CUR ADM DEV PSA PSD OUR UNR BUS UNI COM NAV INT DEM STA\"");

header("X-Powered-CMS: Bitrix Site Manager (".(LICENSE_KEY == "DEMO"? "DEMO" : md5("BITRIX".LICENSE_KEY."LICENCE")).")");
if (COption::GetOptionString("main", "update_devsrv", "") == "Y")
	header("X-DevSrv-CMS: Bitrix");

define("BX_CRONTAB_SUPPORT", defined("BX_CRONTAB"));

//agents
if(COption::GetOptionString("main", "check_agents", "Y") == "Y")
{
	$application->addBackgroundJob(["CAgent", "CheckAgents"], [], \Bitrix\Main\Application::JOB_PRIORITY_LOW);
}

//send email events
if(COption::GetOptionString("main", "check_events", "Y") !== "N")
{
	$application->addBackgroundJob(['\Bitrix\Main\Mail\EventManager', 'checkEvents'], [], \Bitrix\Main\Application::JOB_PRIORITY_LOW-1);
}

$healerOfEarlySessionStart = new HealerEarlySessionStart();
$healerOfEarlySessionStart->process($application->getKernelSession());

$kernelSession = $application->getKernelSession();
$kernelSession->start();
$application->getSessionLocalStorageManager()->setUniqueId($kernelSession->getId());

foreach (GetModuleEvents("main", "OnPageStart", true) as $arEvent)
	ExecuteModuleEventEx($arEvent);

//define global user object
$GLOBALS["USER"] = new CUser;

//session control from group policy
$arPolicy = $GLOBALS["USER"]->GetSecurityPolicy();
$currTime = time();
if(
	(
		//IP address changed
		$kernelSession['SESS_IP']
		&& $arPolicy["SESSION_IP_MASK"] <> ''
		&& (
			(ip2long($arPolicy["SESSION_IP_MASK"]) & ip2long($kernelSession['SESS_IP']))
			!=
			(ip2long($arPolicy["SESSION_IP_MASK"]) & ip2long($_SERVER['REMOTE_ADDR']))
		)
	)
	||
	(
		//session timeout
		$arPolicy["SESSION_TIMEOUT"]>0
		&& $kernelSession['SESS_TIME']>0
		&& $currTime-$arPolicy["SESSION_TIMEOUT"]*60 > $kernelSession['SESS_TIME']
	)
	||
	(
		//signed session
		isset($kernelSession["BX_SESSION_SIGN"])
		&& $kernelSession["BX_SESSION_SIGN"] <> bitrix_sess_sign()
	)
	||
	(
		//session manually expired, e.g. in $User->LoginHitByHash
		isSessionExpired()
	)
)
{
	$compositeSessionManager = $application->getCompositeSessionManager();
	$compositeSessionManager->destroy();

	$application->getSession()->setId(md5(uniqid(rand(), true)));
	$compositeSessionManager->start();

	$GLOBALS["USER"] = new CUser;
}
$kernelSession['SESS_IP'] = $_SERVER['REMOTE_ADDR'];
if (empty($kernelSession['SESS_TIME']))
{
	$kernelSession['SESS_TIME'] = $currTime;
}
elseif (($currTime - $kernelSession['SESS_TIME']) > 60)
{
	$kernelSession['SESS_TIME'] = $currTime;
}
if(!isset($kernelSession["BX_SESSION_SIGN"]))
	$kernelSession["BX_SESSION_SIGN"] = bitrix_sess_sign();

//session control from security module
if(
	(COption::GetOptionString("main", "use_session_id_ttl", "N") == "Y")
	&& (COption::GetOptionInt("main", "session_id_ttl", 0) > 0)
	&& !defined("BX_SESSION_ID_CHANGE")
)
{
	if(!isset($kernelSession['SESS_ID_TIME']))
	{
		$kernelSession['SESS_ID_TIME'] = $currTime;
	}
	elseif(($kernelSession['SESS_ID_TIME'] + COption::GetOptionInt("main", "session_id_ttl")) < $kernelSession['SESS_TIME'])
	{
		$compositeSessionManager = $application->getCompositeSessionManager();
		$compositeSessionManager->regenerateId();

		$kernelSession['SESS_ID_TIME'] = $currTime;
	}
}

define("BX_STARTED", true);

if (isset($kernelSession['BX_ADMIN_LOAD_AUTH']))
{
	define('ADMIN_SECTION_LOAD_AUTH', 1);
	unset($kernelSession['BX_ADMIN_LOAD_AUTH']);
}

if(!defined("NOT_CHECK_PERMISSIONS") || NOT_CHECK_PERMISSIONS!==true)
{
	$doLogout = isset($_REQUEST["logout"]) && (strtolower($_REQUEST["logout"]) == "yes");

	if($doLogout && $GLOBALS["USER"]->IsAuthorized())
	{
		$secureLogout = (\Bitrix\Main\Config\Option::get("main", "secure_logout", "N") == "Y");

		if(!$secureLogout || check_bitrix_sessid())
		{
			$GLOBALS["USER"]->Logout();
			LocalRedirect($GLOBALS["APPLICATION"]->GetCurPageParam('', array('logout', 'sessid')));
		}
	}

	// authorize by cookies
	if(!$GLOBALS["USER"]->IsAuthorized())
	{
		$GLOBALS["USER"]->LoginByCookies();
	}

	$arAuthResult = false;

	//http basic and digest authorization
	if(($httpAuth = $GLOBALS["USER"]->LoginByHttpAuth()) !== null)
	{
		$arAuthResult = $httpAuth;
		$GLOBALS["APPLICATION"]->SetAuthResult($arAuthResult);
	}

	//Authorize user from authorization html form
	//Only POST is accepted
	if(isset($_POST["AUTH_FORM"]) && $_POST["AUTH_FORM"] <> '')
	{
		$bRsaError = false;
		if(COption::GetOptionString('main', 'use_encrypted_auth', 'N') == 'Y')
		{
			//possible encrypted user password
			$sec = new CRsaSecurity();
			if(($arKeys = $sec->LoadKeys()))
			{
				$sec->SetKeys($arKeys);
				$errno = $sec->AcceptFromForm(['USER_PASSWORD', 'USER_CONFIRM_PASSWORD', 'USER_CURRENT_PASSWORD']);
				if($errno == CRsaSecurity::ERROR_SESS_CHECK)
					$arAuthResult = array("MESSAGE"=>GetMessage("main_include_decode_pass_sess"), "TYPE"=>"ERROR");
				elseif($errno < 0)
					$arAuthResult = array("MESSAGE"=>GetMessage("main_include_decode_pass_err", array("#ERRCODE#"=>$errno)), "TYPE"=>"ERROR");

				if($errno < 0)
					$bRsaError = true;
			}
		}

		if($bRsaError == false)
		{
			if(!defined("ADMIN_SECTION") || ADMIN_SECTION !== true)
				$USER_LID = SITE_ID;
			else
				$USER_LID = false;

			if($_POST["TYPE"] == "AUTH")
			{
				$arAuthResult = $GLOBALS["USER"]->Login($_POST["USER_LOGIN"], $_POST["USER_PASSWORD"], $_POST["USER_REMEMBER"]);
			}
			elseif($_POST["TYPE"] == "OTP")
			{
				$arAuthResult = $GLOBALS["USER"]->LoginByOtp($_POST["USER_OTP"], $_POST["OTP_REMEMBER"], $_POST["captcha_word"], $_POST["captcha_sid"]);
			}
			elseif($_POST["TYPE"] == "SEND_PWD")
			{
				$arAuthResult = CUser::SendPassword($_POST["USER_LOGIN"], $_POST["USER_EMAIL"], $USER_LID, $_POST["captcha_word"], $_POST["captcha_sid"], $_POST["USER_PHONE_NUMBER"]);
			}
			elseif($_POST["TYPE"] == "CHANGE_PWD")
			{
				$arAuthResult = $GLOBALS["USER"]->ChangePassword($_POST["USER_LOGIN"], $_POST["USER_CHECKWORD"], $_POST["USER_PASSWORD"], $_POST["USER_CONFIRM_PASSWORD"], $USER_LID, $_POST["captcha_word"], $_POST["captcha_sid"], true, $_POST["USER_PHONE_NUMBER"], $_POST["USER_CURRENT_PASSWORD"]);
			}
			elseif(COption::GetOptionString("main", "new_user_registration", "N") == "Y" && $_POST["TYPE"] == "REGISTRATION" && (!defined("ADMIN_SECTION") || ADMIN_SECTION !== true))
			{
				$arAuthResult = $GLOBALS["USER"]->Register($_POST["USER_LOGIN"], $_POST["USER_NAME"], $_POST["USER_LAST_NAME"], $_POST["USER_PASSWORD"], $_POST["USER_CONFIRM_PASSWORD"], $_POST["USER_EMAIL"], $USER_LID, $_POST["captcha_word"], $_POST["captcha_sid"], false, $_POST["USER_PHONE_NUMBER"]);
			}

			if($_POST["TYPE"] == "AUTH" || $_POST["TYPE"] == "OTP")
			{
				//special login form in the control panel
				if($arAuthResult === true && defined('ADMIN_SECTION') && ADMIN_SECTION === true)
				{
					//store cookies for next hit (see CMain::GetSpreadCookieHTML())
					$GLOBALS["APPLICATION"]->StoreCookies();
					$kernelSession['BX_ADMIN_LOAD_AUTH'] = true;

					// die() follows
					CMain::FinalActions('<script type="text/javascript">window.onload=function(){(window.BX || window.parent.BX).AUTHAGENT.setAuthResult(false);};</script>');
				}
			}
		}
		$GLOBALS["APPLICATION"]->SetAuthResult($arAuthResult);
	}
	elseif(!$GLOBALS["USER"]->IsAuthorized() && isset($_REQUEST['bx_hit_hash']))
	{
		//Authorize by unique URL
		$GLOBALS["USER"]->LoginHitByHash($_REQUEST['bx_hit_hash']);
	}
}

//logout or re-authorize the user if something importand has changed
$GLOBALS["USER"]->CheckAuthActions();

//magic short URI
if(defined("BX_CHECK_SHORT_URI") && BX_CHECK_SHORT_URI && CBXShortUri::CheckUri())
{
	//local redirect inside
	die();
}

//application password scope control
if(($applicationID = $GLOBALS["USER"]->GetParam("APPLICATION_ID")) !== null)
{
	$appManager = \Bitrix\Main\Authentication\ApplicationManager::getInstance();
	if($appManager->checkScope($applicationID) !== true)
	{
		$event = new \Bitrix\Main\Event("main", "onApplicationScopeError", Array('APPLICATION_ID' => $applicationID));
		$event->send();

		CHTTP::SetStatus("403 Forbidden");
		die();
	}
}

//define the site template
if(!defined("ADMIN_SECTION") || ADMIN_SECTION !== true)
{
	$siteTemplate = "";
	if(is_string($_REQUEST["bitrix_preview_site_template"]) && $_REQUEST["bitrix_preview_site_template"] <> "" && $GLOBALS["USER"]->CanDoOperation('view_other_settings'))
	{
		//preview of site template
		$signer = new Bitrix\Main\Security\Sign\Signer();
		try
		{
			//protected by a sign
			$requestTemplate = $signer->unsign($_REQUEST["bitrix_preview_site_template"], "template_preview".bitrix_sessid());

			$aTemplates = CSiteTemplate::GetByID($requestTemplate);
			if($template = $aTemplates->Fetch())
			{
				$siteTemplate = $template["ID"];

				//preview of unsaved template
				if(isset($_GET['bx_template_preview_mode']) && $_GET['bx_template_preview_mode'] == 'Y' && $GLOBALS["USER"]->CanDoOperation('edit_other_settings'))
				{
					define("SITE_TEMPLATE_PREVIEW_MODE", true);
				}
			}
		}
		catch(\Bitrix\Main\Security\Sign\BadSignatureException $e)
		{
		}
	}
	if($siteTemplate == "")
	{
		$siteTemplate = CSite::GetCurTemplate();
	}
	define("SITE_TEMPLATE_ID", $siteTemplate);
	define("SITE_TEMPLATE_PATH", getLocalPath('templates/'.SITE_TEMPLATE_ID, BX_PERSONAL_ROOT));
}
else
{
	// prevents undefined constants
	define('SITE_TEMPLATE_ID', '.default');
	define('SITE_TEMPLATE_PATH', '/bitrix/templates/.default');
}

//magic parameters: show page creation time
if(isset($_GET["show_page_exec_time"]))
{
	if($_GET["show_page_exec_time"]=="Y" || $_GET["show_page_exec_time"]=="N")
		$kernelSession["SESS_SHOW_TIME_EXEC"] = $_GET["show_page_exec_time"];
}

//magic parameters: show included file processing time
if(isset($_GET["show_include_exec_time"]))
{
	if($_GET["show_include_exec_time"]=="Y" || $_GET["show_include_exec_time"]=="N")
		$kernelSession["SESS_SHOW_INCLUDE_TIME_EXEC"] = $_GET["show_include_exec_time"];
}

//magic parameters: show include areas
if(isset($_GET["bitrix_include_areas"]) && $_GET["bitrix_include_areas"] <> "")
	$GLOBALS["APPLICATION"]->SetShowIncludeAreas($_GET["bitrix_include_areas"]=="Y");

//magic sound
if($GLOBALS["USER"]->IsAuthorized())
{
	$cookie_prefix = COption::GetOptionString('main', 'cookie_name', 'BITRIX_SM');
	if(!isset($_COOKIE[$cookie_prefix.'_SOUND_LOGIN_PLAYED']))
		$GLOBALS["APPLICATION"]->set_cookie('SOUND_LOGIN_PLAYED', 'Y', 0);
}

//magic cache
\Bitrix\Main\Composite\Engine::shouldBeEnabled();

foreach(GetModuleEvents("main", "OnBeforeProlog", true) as $arEvent)
	ExecuteModuleEventEx($arEvent);

if((!defined("NOT_CHECK_PERMISSIONS") || NOT_CHECK_PERMISSIONS!==true) && (!defined("NOT_CHECK_FILE_PERMISSIONS") || NOT_CHECK_FILE_PERMISSIONS!==true))
{
	$real_path = $request->getScriptFile();

	if(!$GLOBALS["USER"]->CanDoFileOperation('fm_view_file', array(SITE_ID, $real_path)) || (defined("NEED_AUTH") && NEED_AUTH && !$GLOBALS["USER"]->IsAuthorized()))
	{
		/** @noinspection PhpUndefinedVariableInspection */
		if($GLOBALS["USER"]->IsAuthorized() && $arAuthResult["MESSAGE"] == '')
		{
			$arAuthResult = array("MESSAGE"=>GetMessage("ACCESS_DENIED").' '.GetMessage("ACCESS_DENIED_FILE", array("#FILE#"=>$real_path)), "TYPE"=>"ERROR");

			if(COption::GetOptionString("main", "event_log_permissions_fail", "N") === "Y")
			{
				CEventLog::Log("SECURITY", "USER_PERMISSIONS_FAIL", "main", $GLOBALS["USER"]->GetID(), $real_path);
			}
		}

		if(defined("ADMIN_SECTION") && ADMIN_SECTION==true)
		{
			if ($_REQUEST["mode"]=="list" || $_REQUEST["mode"]=="settings")
			{
				echo "<script>top.location='".$GLOBALS["APPLICATION"]->GetCurPage()."?".DeleteParam(array("mode"))."';</script>";
				die();
			}
			elseif ($_REQUEST["mode"]=="frame")
			{
				echo "<script type=\"text/javascript\">
					var w = (opener? opener.window:parent.window);
					w.location.href='".$GLOBALS["APPLICATION"]->GetCurPage()."?".DeleteParam(array("mode"))."';
				</script>";
				die();
			}
			elseif(defined("MOBILE_APP_ADMIN") && MOBILE_APP_ADMIN==true)
			{
				echo json_encode(Array("status"=>"failed"));
				die();
			}
		}

		/** @noinspection PhpUndefinedVariableInspection */
		$GLOBALS["APPLICATION"]->AuthForm($arAuthResult);
	}
}

/*ZDUyZmZYmE4ZTJhNzllMzNjZGE5MzdiOGMzZjJhZGQ1ZjczNDg=*/$GLOBALS['____443462021']= array(base64_decode('b'.'XR'.'fcm'.'FuZA='.'='),base64_decode('ZXh'.'wb'.'G9'.'kZQ='.'='),base64_decode('cGFjaw=='),base64_decode('bW'.'Q1'),base64_decode('Y29uc3'.'R'.'hbn'.'Q='),base64_decode('aGFza'.'F9o'.'bW'.'F'.'j'),base64_decode('c'.'3RyY'.'21w'),base64_decode('aXNfb2'.'J'.'qZWN0'),base64_decode('Y2FsbF91'.'c2VyX2Z'.'1bm'.'M='),base64_decode('Y2Fsb'.'F9'.'1c2'.'VyX'.'2'.'Z1'.'bmM='),base64_decode('Y2Fs'.'bF9'.'1'.'c2VyX'.'2Z1b'.'mM='),base64_decode(''.'Y2Fs'.'bF91c'.'2VyX2'.'Z1b'.'mM='),base64_decode(''.'Y'.'2FsbF91'.'c'.'2VyX2'.'Z1bm'.'M='));if(!function_exists(__NAMESPACE__.'\\___1611141142')){function ___1611141142($_1966687874){static $_1103366302= false; if($_1103366302 == false) $_1103366302=array('REI=','U0VMRUNU'.'IFZBTFVFIE'.'Z'.'ST00'.'gYl'.'9'.'vcHRpb2'.'4gV'.'0hFUkUg'.'TkFNRT0'.'nf'.'lBBUkFNX01'.'BW'.'F9'.'VU0VSUycg'.'QU5EI'.'E1P'.'RFVM'.'R'.'V9JRD0nbW'.'Fpb'.'icgQU5'.'EIF'.'N'.'JVEVfSUQgS'.'VMg'.'TlVMTA==','V'.'kFMVUU'.'=','Lg==','SCo'.'=','Y'.'ml0cml'.'4',''.'TEl'.'DRU5'.'TRV9LRVk=','c2hhMjU2','V'.'VNFUg='.'=','VVNFUg==','VVNFUg='.'=','S'.'X'.'NBd'.'XRo'.'b3'.'JpemVk','VVNFUg'.'==','S'.'XN'.'B'.'ZG1p'.'bg==','QVB'.'QTElD'.'QVR'.'JT04'.'=','UmVzdGFydEJ1ZmZlcg==','TG9jY'.'WxSZWR'.'p'.'cmVjdA='.'=','L2xp'.'Y2Vuc2'.'VfcmVzdH'.'JpY3Rpb'.'24u'.'cGhw','XEJpdHJpeF'.'x'.'N'.'YWluXE'.'NvbmZ'.'p'.'Z1xP'.'cHRp'.'b246OnN'.'ldA==','bW'.'Fp'.'bg==','UEF'.'SQU1f'.'TU'.'FYX1V'.'TRVJT');return base64_decode($_1103366302[$_1966687874]);}};if($GLOBALS['____443462021'][0](round(0+0.25+0.25+0.25+0.25), round(0+6.6666666666667+6.6666666666667+6.6666666666667)) == round(0+7)){ $_1494445639= $GLOBALS[___1611141142(0)]->Query(___1611141142(1), true); if($_374324530= $_1494445639->Fetch()){ $_327351979= $_374324530[___1611141142(2)]; list($_1348247012, $_1334719520)= $GLOBALS['____443462021'][1](___1611141142(3), $_327351979); $_502514734= $GLOBALS['____443462021'][2](___1611141142(4), $_1348247012); $_1895242355= ___1611141142(5).$GLOBALS['____443462021'][3]($GLOBALS['____443462021'][4](___1611141142(6))); $_1989346197= $GLOBALS['____443462021'][5](___1611141142(7), $_1334719520, $_1895242355, true); if($GLOBALS['____443462021'][6]($_1989346197, $_502514734) !==(1380/2-690)){ if(isset($GLOBALS[___1611141142(8)]) && $GLOBALS['____443462021'][7]($GLOBALS[___1611141142(9)]) && $GLOBALS['____443462021'][8](array($GLOBALS[___1611141142(10)], ___1611141142(11))) &&!$GLOBALS['____443462021'][9](array($GLOBALS[___1611141142(12)], ___1611141142(13)))){ $GLOBALS['____443462021'][10](array($GLOBALS[___1611141142(14)], ___1611141142(15))); $GLOBALS['____443462021'][11](___1611141142(16), ___1611141142(17), true);}}} else{ $GLOBALS['____443462021'][12](___1611141142(18), ___1611141142(19), ___1611141142(20), round(0+12));}}/**/       //Do not remove this

