<?php

namespace Bitrix\Socialnetwork\Component\LogList;

class Util extends \Bitrix\Socialnetwork\Component\LogListCommon\Util
{
	public static function checkEmptyParamInteger(&$params, $paramName, $defaultValue): void
	{
		$params[$paramName] = (isset($params[$paramName]) && (int)$params[$paramName] > 0 ? (int)$params[$paramName] : $defaultValue);
	}

	public static function checkEmptyParamString(&$params, $paramName, $defaultValue): void
	{
		$params[$paramName] = (isset($params[$paramName]) && trim($params[$paramName]) !== '' ? trim($params[$paramName]) : $defaultValue);
	}

	public static function checkUserAuthorized(): bool
	{
		global $USER;
		return (isset($USER) && is_object($USER) ? $USER->isAuthorized() : false);
	}
}
