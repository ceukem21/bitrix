<?php
$MESS["TASKS_GLDA_ACCESS_DENIED"] = "Only the Bitrix24 administrators can access action properties.";
$MESS["TASKS_GLDA_ELEMENT_ID"] = "Task ID";
$MESS["TASKS_GLDA_ERROR_ELEMENT_ID"] = "Task ID is required";
$MESS["TASKS_GLDA_ERROR_EMPTY_DOCUMENT"] = "Cannot get document information";
$MESS["TASKS_GLDA_ERROR_FIELDS"] = "No document fields selected";
$MESS["TASKS_GLDA_FIELDS_LABEL"] = "Select fields";
