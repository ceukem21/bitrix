<?php

namespace Bitrix\Crm\Controller;

use Bitrix\Crm\Component\EntityDetails\FactoryBased;
use Bitrix\Crm\Field;
use Bitrix\Crm\Service;
use Bitrix\Crm\Service\Container;
use Bitrix\Crm\Service\EditorAdapter;
use Bitrix\Crm\Settings\RestSettings;
use Bitrix\Main\Component\ParameterSigner;
use Bitrix\Main\Engine\ActionFilter\Csrf;
use Bitrix\Main\Engine\Response\BFile;
use Bitrix\Main\Engine\Response\Component;
use Bitrix\Main\Engine\Response\DataType\Page;
use Bitrix\Main\Error;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\UI\PageNavigation;

class Item extends Base
{
	public function configureActions(): array
	{
		$configureActions = parent::configureActions();
		$configureActions['getFile'] = [
			'-prefilters' => [
				Csrf::class,
			],
		];

		return $configureActions;
	}

	protected function getFactory(int $entityTypeId): ?Service\Factory
	{
		$factory = Container::getInstance()->getFactory($entityTypeId);
		if (!$factory)
		{
			$this->addError(new Error(
					Loc::getMessage('CRM_TYPE_TYPE_NOT_FOUND'),
					ErrorCode::NOT_FOUND)
			);
		}

		return $factory;
	}

	/**
	 * Return data about item by $id.
	 *
	 * @param int $id
	 * @return array|null
	 */
	public function getAction(int $id, int $entityTypeId): ?array
	{
		$factory = $this->getFactory($entityTypeId);
		if (!$factory)
		{
			return null;
		}
		$item = $factory->getItem($id);
		if (!$item)
		{
			$this->addError(new Error(
				Loc::getMessage('CRM_TYPE_ITEM_NOT_FOUND'),
				ErrorCode::NOT_FOUND
			));
			return null;
		}
		if (!Container::getInstance()->getUserPermissions()->canReadItem($item))
		{
			$this->addError(new Error(
				Loc::getMessage('CRM_COMMON_READ_ACCESS_DENIED'),
				ErrorCode::ACCESS_DENIED
			));
			return null;
		}

		return [
			'item' => $this->getJsonForItems($factory, [$item])[$item->getId()],
		];
	}

	/**
	 * Return list of items.
	 *
	 * @param array|null $order
	 * @param array|null $filter
	 * @param PageNavigation|null $pageNavigation
	 * @return Page|null
	 */
	public function listAction(
		int $entityTypeId,
		array $order = null,
		array $filter = null,
		PageNavigation $pageNavigation = null
	): ?Page
	{
		$factory = $this->getFactory($entityTypeId);
		if (!$factory)
		{
			return null;
		}
		$parameters = [];
		$parameters['filter'] = $this->convertKeysToUpper((array)$filter);
		$parameters['filter'] = $this->prepareFilter($factory, $parameters['filter']);
		if(is_array($order))
		{
			$parameters['order'] = $this->convertKeysToUpper($order);
		}

		if($pageNavigation)
		{
			$parameters['offset'] = $pageNavigation->getOffset();
			$parameters['limit'] = $pageNavigation->getLimit();
		}

		$items = $factory->getItemsFilteredByPermissions($parameters);
		$items = array_values($this->getJsonForItems($factory, $items));

		return new Page(
			'items',
			$items,
			function() use($parameters, $factory) {
				return $factory->getItemsCountFilteredByPermissions($parameters['filter']);
			}
		);
	}

	/**
	 * Delete item by $id.
	 *
	 * @param int $id
	 * @return array|null
	 */
	public function deleteAction(int $entityTypeId, int $id): ?array
	{
		$factory = $this->getFactory($entityTypeId);
		if (!$factory)
		{
			return null;
		}
		$item = $factory->getItem($id);
		if (!$item)
		{
			$this->addError(new Error(
				Loc::getMessage('CRM_TYPE_ITEM_NOT_FOUND'),
				ErrorCode::NOT_FOUND
			));
			return null;
		}
		if (!Container::getInstance()->getUserPermissions()->canDeleteItem($item))
		{
			$this->addError(new Error(
				Loc::getMessage('CRM_COMMON_ERROR_ACCESS_DENIED'),
				ErrorCode::ACCESS_DENIED
			));
			return null;
		}

		$categoryId = $item->getCategoryId();

		$operation = $factory->getDeleteOperation($item);
		$result = $operation->launch();
		if (!$result->isSuccess())
		{
			$this->addErrors($result->getErrors());
			return null;
		}

		if ($this->getScope() === static::SCOPE_AJAX)
		{
			return [
				'redirectUrl' => Container::getInstance()->getRouter()
					->getItemListUrlInCurrentView($entityTypeId, $categoryId),
			];
		}

		return [];
	}

	/**
	 * Process $fields for $item by fields $collection. This method do not perform saving, only set values in $item.
	 *
	 * @param \Bitrix\Crm\Item $item
	 * @param array $fields
	 * @param Field\Collection $collection
	 */
	public function processFields(
		\Bitrix\Crm\Item $item,
		array $fields,
		Field\Collection $collection
	): void
	{
		$fields = $collection->removeHiddenValues($fields);

		foreach($collection as $field)
		{
			$fieldName = $field->getName();
			if (!array_key_exists($fieldName, $fields))
			{
				continue;
			}

			if (empty($fields[$fieldName]))
			{
				$item->set($fieldName, null);
				continue;
			}

			if ($field->isFileUserField())
			{
				$this->processFileField($field, $item, $fields[$fieldName]);
			}
			elseif (
				$this->getScope() === self::SCOPE_REST
				&& (
					$field->getType() === Field::TYPE_DATE
					|| $field->getType() === Field::TYPE_DATETIME
				)
			)
			{
				if ($field->getType() === Field::TYPE_DATETIME)
				{
					$convertDateMethod = 'unConvertDateTime';
				}
				else
				{
					$convertDateMethod = 'unConvertDate';
				}
				if($field->isMultiple())
				{
					$result = [];
					$value = (array)$fields[$fieldName];
					foreach($value as $date)
					{
						$result[] = \CRestUtil::$convertDateMethod($date);
					}
					$item->set($fieldName, $result);
				}
				else
				{
					$item->set($fieldName, \CRestUtil::$convertDateMethod($fields[$fieldName]));
				}
			}
			else
			{
				$value = $fields[$fieldName];
				if ($field->getType() === Field::TYPE_BOOLEAN)
				{
					$value = $this->prepareBooleanFieldValue($value);
				}
				$item->set($fieldName, $value);
			}
		}
	}

	public function processFileField(Field $field, \Bitrix\Crm\Item $item, $fileData): void
	{
		$fieldName = $field->getName();
		if ($field->isMultiple())
		{
			$fileData = (array)$fileData;

			$result = [];
			$currentFiles = array_flip($item->get($fieldName) ?? []);
			foreach ($fileData as $file)
			{
				$fileId = (int)$file['ID'];
				if ($fileId > 0)
				{
					if (isset($currentFiles[$fileId]))
					{
						$result[] = $fileId;
					}

					continue;
				}

				$fileId = $this->uploadFile($field, $file);
				if ($fileId > 0)
				{
					$result[] = $fileId;
				}
			}

			$item->set($fieldName, $result);
		}
		else
		{
			if (isset($fileData['ID']))
			{
				if ((int)$fileData['ID'] === $item->get($fieldName))
				{
					return;
				}

				$fileId = 0;
			}
			else
			{
				$fileId = $this->uploadFile($field, $fileData);
			}
			$item->set($fieldName, $fileId);
		}
	}

	protected function prepareBooleanFieldValue($value): bool
	{
		if (is_bool($value))
		{
			return $value;
		}
		if ($value === 'true' || $value === 'y' || $value === 'Y')
		{
			return true;
		}

		return false;
	}

	/**
	 * Add new item with $fields.
	 *
	 * @param array $fields
	 * @return array|null
	 */
	public function addAction(int $entityTypeId, array $fields): ?array
	{
		$factory = $this->getFactory($entityTypeId);
		if (!$factory)
		{
			return null;
		}
		$item = $factory->createItem();

		$fields = $this->convertKeysToUpper($fields);
		$this->processFields($item, $fields, $factory->getFieldsCollection());

		if (!Container::getInstance()->getUserPermissions()->canAddItem($item))
		{
			$this->addError(new Error(
				Loc::getMessage('CRM_COMMON_ERROR_ACCESS_DENIED'),
				ErrorCode::ACCESS_DENIED
			));
			return null;
		}

		$operation = $factory->getAddOperation($item);
		if (
			$this->getScope() === static::SCOPE_REST
			&& !RestSettings::getCurrent()->isRequiredUserFieldCheckEnabled()
		)
		{
			$operation->disableCheckRequiredUserFields();
		}
		$result = $operation->launch();
		if ($result->isSuccess())
		{
			Container::getInstance()->getParentFieldManager()->saveItemRelations($item, $fields);

			return [
				'item' => $this->getJsonForItems($factory, [$operation->getItem()])[$item->getId()],
			];
		}

		$this->addErrors($result->getErrors());

		return null;
	}

	/**
	 * Update item by $id with $fields.
	 *
	 * @param int $id
	 * @param array $fields
	 * @return array|null
	 */
	public function updateAction(int $entityTypeId, int $id, array $fields): ?array
	{
		$factory = $this->getFactory($entityTypeId);
		if (!$factory)
		{
			return null;
		}
		$item = $factory->getItem($id);
		if (!$item)
		{
			$this->addError(new Error(
				Loc::getMessage('CRM_TYPE_ITEM_NOT_FOUND'),
				ErrorCode::NOT_FOUND
			));
			return null;
		}
		if (!Container::getInstance()->getUserPermissions()->canUpdateItem($item))
		{
			$this->addError(new Error(
				Loc::getMessage('CRM_COMMON_ERROR_ACCESS_DENIED'),
				ErrorCode::ACCESS_DENIED
			));
			return null;
		}

		$fields = $this->convertKeysToUpper($fields);
		$this->processFields($item, $fields, $factory->getFieldsCollection());
		$operation = $factory->getUpdateOperation($item);
		if (
			$this->getScope() === static::SCOPE_REST
			&& !RestSettings::getCurrent()->isRequiredUserFieldCheckEnabled()
		)
		{
			$operation->disableCheckRequiredUserFields();
		}
		$result = $operation->launch();
		if ($result->isSuccess())
		{
			Container::getInstance()->getParentFieldManager()->saveItemRelations($item, $fields);

			$item = $operation->getItem();

			return [
				'item' => $this->getJsonForItems($factory, [$operation->getItem()])[$item->getId()],
			];
		}

		$this->addErrors($result->getErrors());

		return null;
	}

	/**
	 * Return editor for item by $id.
	 *
	 * @param int $id
	 * @param string|null $guid
	 * @param string|null $configId
	 * @param int|null $categoryId
	 * @param string|null $stageId
	 * @param array $params
	 * @return Component|null
	 */
	public function getEditorAction(
		int $entityTypeId,
		int $id,
		string $guid = null,
		string $configId = null,
		int $categoryId = null,
		string $stageId = null,
		array $params = []
	): ?Component
	{
		$factory = $this->getFactory($entityTypeId);
		if (!$factory)
		{
			return null;
		}
		$componentName = $params['componentName']
			?? Container::getInstance()->getRouter()->getItemDetailComponentName($entityTypeId);
		if (!$componentName)
		{
			$this->addError(new Error('Component for entity ' . $entityTypeId . ' not found'));
			return null;
		}

		$componentClassName = \CBitrixComponent::includeComponentClass($componentName);
		$component = new $componentClassName;
		if (!($component instanceof FactoryBased))
		{
			$this->addError(new Error('Component for entity ' . $entityTypeId . ' not found'));
			return null;
		}
		$component->initComponent($componentName);
		$component->arParams = [
			'ENTITY_TYPE_ID' => $entityTypeId,
			'ENTITY_ID' => $id,
			'categoryId' => $categoryId,
		];

		$component->init();
		if (!empty($component->getErrors()))
		{
			$this->addErrors($component->getErrors());
			return null;
		}

		$component->initializeEditorAdapter();

		$editorConfig = $component->getEditorConfig();
		$editorConfig['ENTITY_CONFIG'] = $component->getInlineEditorEntityConfig();

		if ($stageId)
		{
			$editorConfig['CONTEXT']['STAGE_ID'] = $stageId;
		}

		$forceDefaultConfig = $params['forceDefaultConfig'] ?? 'N';
		$editorConfig['FORCE_DEFAULT_CONFIG'] = ($forceDefaultConfig === 'Y');
		$editorConfig['IS_EMBEDDED'] = true;
		$editorConfig['GUID'] = $guid ?? $editorConfig['GUID'];
		$editorConfig['CONFIG_ID'] = $configId ?? $editorConfig['CONFIG_ID'];

		$editorConfig['COMPONENT_AJAX_DATA']['SIGNED_PARAMETERS'] = ParameterSigner::signParameters(
			$component->getName(),
			$component->arParams
		);

		$disabledOptions = [
			'ENABLE_SECTION_EDIT',
			'ENABLE_SECTION_CREATION',
			'ENABLE_SECTION_DRAG_DROP',
			'ENABLE_FIELD_DRAG_DROP',
			'ENABLE_MODE_TOGGLE',
			'ENABLE_TOOL_PANEL',
			'ENABLE_BOTTOM_PANEL',
			'ENABLE_USER_FIELD_CREATION',
			'ENABLE_PAGE_TITLE_CONTROLS',
			'ENABLE_FIELDS_CONTEXT_MENU',
			'ENABLE_PERSONAL_CONFIGURATION_UPDATE',
			'ENABLE_COMMON_CONFIGURATION_UPDATE',
			'ENABLE_CONFIG_SCOPE_TOGGLE',
			'ENABLE_SETTINGS_FOR_ALL',
			'ENABLE_REQUIRED_FIELDS_INJECTION',
		];
		foreach ($disabledOptions as $option)
		{
			$editorConfig[$option] = $params[$option] ?? false;
		}

		$editorConfig['ENABLE_USER_FIELD_MANDATORY_CONTROL'] = true;
		$editorConfig['ENABLE_AJAX_FORM'] = true;
		$editorConfig['INITIAL_MODE'] = 'edit';

		$requiredFields = array_unique($params['requiredFields'] ?? []);
		$entityConfig = $editorConfig['ENTITY_CONFIG'];
		if (!empty($requiredFields))
		{
			$entityConfig = [
				[
					'elements' => [],
				],
			];

			foreach ($requiredFields as $field)
			{
				$entityConfig[0]['elements'][] = ['name' => $field];
			}

			$editorConfig['ENTITY_FIELDS'] = EditorAdapter::markFieldsAsRequired($editorConfig['ENTITY_FIELDS'], $requiredFields);
		}

		$editorConfig['ENTITY_CONFIG'] = EditorAdapter::combineConfigIntoOneSection($entityConfig, $params['title'] ?? '');

		return new Component('bitrix:crm.entity.editor', '', $editorConfig);
	}

	/**
	 * Prepare filter for getList.
	 *
	 * @param array $filter
	 * @return array
	 */
	public function prepareFilter(Service\Factory $factory, array $filter): array
	{
		if($this->getScope() === static::SCOPE_REST)
		{
			$this->prepareDateTimeFieldsForFilter($filter, $factory->getFieldsCollection());
		}

		return $this->removeDotsFromKeys($filter);
	}

	/**
	 * Return information about fields.
	 *
	 * @return array|null
	 */
	public function fieldsAction(int $entityTypeId): ?array
	{
		$factory = $this->getFactory($entityTypeId);
		if (!$factory)
		{
			return null;
		}
		if (!Container::getInstance()->getUserPermissions()->checkReadPermissions($entityTypeId))
		{
			$this->addError(new Error(
				Loc::getMessage('CRM_COMMON_READ_ACCESS_DENIED'),
				ErrorCode::ACCESS_DENIED
			));

			return null;
		}

		$fieldsInfo = $factory->getFieldsInfo() + $factory->getUserFieldsInfo();

		return [
			'fields' => $this->prepareFieldsInfo($fieldsInfo),
		];
	}

	/**
	 * Return file content of item with $id by $fieldName and $file_id.
	 *
	 * @param int $id
	 * @param string $fieldName
	 * @param int $fileId
	 * @param int|null $entityTypeId
	 * @return BFile|null
	 */
	public function getFileAction(
		int $entityTypeId,
		int $id,
		string $fieldName,
		int $fileId
	): ?BFile
	{
		$factory = $this->getFactory($entityTypeId);
		if (!$factory)
		{
			return null;
		}

		$item = $factory->getItem($id);
		if (!$item)
		{
			$this->addError(new Error(
				Loc::getMessage('CRM_TYPE_ITEM_NOT_FOUND'),
				ErrorCode::NOT_FOUND
			));
			return null;
		}
		if (!Container::getInstance()->getUserPermissions()->canReadItem($item))
		{
			$this->addError(new Error(
				Loc::getMessage('CRM_COMMON_READ_ACCESS_DENIED'),
				ErrorCode::ACCESS_DENIED
			));
			return null;
		}

		$field = $factory->getFieldsCollection()->getField($fieldName);
		if (!$field || !$field->isFileUserField())
		{
			$this->addError(new Error('Field ' . $fieldName . ' is not a file field'));
			return null;
		}
		$value = $item->get($fieldName);
		if(
			($value === $fileId)
			||
			(
				is_array($value)
				&& in_array($fileId, $value, true)
			)
		)
		{
			return BFile::createByFileId($fileId);
		}

		return null;
	}

	/**
	 * @param \Bitrix\Crm\Item[] $items
	 * @return array
	 */
	protected function getJsonForItems(Service\Factory $factory, array $items): array
	{
		$result = [];
		foreach ($items as $item)
		{
			$result[$item->getId()] = $item->jsonSerialize();
		}
		if (empty($result))
		{
			return $result;
		}

		$parentFieldManager = Container::getInstance()->getParentFieldManager();
		$entityTypeId = $factory->getEntityTypeId();
		$parentFields = $parentFieldManager->getParentFieldsInfo($entityTypeId);
		if (empty($parentFields))
		{
			return $result;
		}
		$parentFieldNames = array_keys($parentFields);
		$casedFieldNames = array_combine($parentFieldNames, $parentFieldNames);
		$casedFieldNames = array_flip($this->convertKeysToCamelCase($casedFieldNames));

		$parentFieldValues = $parentFieldManager->getParentFields(
			array_keys($result),
			$parentFieldNames,
			$entityTypeId
		);
		foreach ($result as $itemId => $itemData)
		{
			foreach ($casedFieldNames as $fieldName)
			{
				$result[$itemId][$fieldName] = null;
			}
		}
		foreach ($parentFieldValues as $itemId => $parents)
		{
			foreach ($parents as $parentValue)
			{
				$result[$itemId][$casedFieldNames[$parentValue['code']]] = $parentValue['id'];
			}
		}

		return $result;
	}
}
