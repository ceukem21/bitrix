<?

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
    die();
}

if(!CModule::IncludeModule('askarasoft.kota')) {
    ShowError("Module askarasoft.kota not installed");
    return;
}
use Bitrix\Main\UI\Filter;
// global $DB;
global $USER, $DB;
// take USER id and data who's logging in
$self_user_id = $USER->GetID();
$self_data_user = CUser::GetByID($self_user_id)->Fetch();

// get request data
$request = \Bitrix\Main\Context::getCurrent()->getRequest();

// initialitation GRID ID
$arResult["GRID_ID"] = $destination . '_kota';

// configure GRID ID in GRID OPTIONS
$grid_options = new CGridOptions ($arResult ["GRID_ID"]);

// SET RULE SORT 'DESCENDING'
$arrSort['sort']['ID'] = "DESC";
$aSort = $grid_options->GetSorting($arrSort);

// PAGINATION MAX 10 data
$aNav = $grid_options-> GetNavParams (array ("nPageSize" => 10));
$navPageSize = $aNav['nPageSize'];

// SET DATA TO RULE SORT
$aSortArg = each($aSort ["sort"]);

// SET FILTER DATA AND SET IT TO VIEWS
$filter = array();
$filterOption = new Bitrix\Main\UI\Filter\Options($arResult ["GRID_ID"]);

$filterData = $filterOption->getFilter(array());
$filter_text = array();

foreach ($filterData as $k => $v) {
    $filter [$k] = $v;
}

$exclude_filter_date = array(
    Filter\DateType::CURRENT_WEEK,
    Filter\DateType::CURRENT_MONTH,
    Filter\DateType::CURRENT_QUARTER,
    Filter\DateType::YESTERDAY,
    Filter\DateType::TOMORROW,
    Filter\DateType::PREV_DAYS,
    Filter\DateType::NEXT_DAYS,
    Filter\DateType::NEXT_WEEK,
    Filter\DateType::NEXT_MONTH,
    Filter\DateType::LAST_MONTH,
    Filter\DateType::LAST_WEEK,
    Filter\DateType::EXACT,
    Filter\DateType::LAST_7_DAYS,
    Filter\DateType::LAST_30_DAYS,
    Filter\DateType::LAST_60_DAYS,
    Filter\DateType::LAST_90_DAYS,
    Filter\DateType::QUARTER, 
);

// SET FILLABLE FILTER DATA
$arResult ["FILTER"] = array (
    array (
        'id' => 'NAMA_KOTA', 
        'name' => 'Nama Kota', 
        'type' => 'text', 
        'default' => true,
    ),
    array(
        'id' => 'CREATED_BY',
        'name' => 'User Pembuat',
        'params'=>array('multiple'=>'Y'),
        'type' => 'custom_entity',
        'selector' => array(
            'TYPE' => 'user',
            'DATA' => array(
                'ID' => 'user',
                'FIELD_ID' => 'CREATED_BY[]'
            )
        ),
        'default' => true
    ),
    array(
        'id' => "CREATED_DATE", 
        'name' => 'Tanggal Terbuat',
        'type' => 'date',
		'exclude' => $exclude_filter_date, 
    ),
    array(
        'id' => "MODIFIED_DATE", 
        'name' => 'Tanggal Perubahan',
        'type' => 'date',
		'exclude' => $exclude_filter_date,
    )
);


// set fillable filter data to grid options 
$aFilter = $grid_options->GetFilter($arResult ["FILTER"]);
$sql_condition = '';
foreach ($filter as $key => $val) { 
    $exp_key = explode('_datesel', $key);
    $key_date = '';
    if($exp_key>1){
        $key_date = $exp_key[0];
    }
    // QUERY LIKE FOR EACH DATA WITH TYPE DATA 'DATE'
    if(!empty($key_date)){
        switch ($key) { 
            case $key_date.'_datesel':
                if($val == 'CURRENT_DAY'){ 
                    $sql_condition .= '(DATE(a.'.$key_date.') = CURDATE()) AND ';
                }
                if($val == 'YEAR'){
                    $val_key1 = "'".$DB->ForSql($filter[$key_date.'_year'])."'";
                    $sql_condition .= '(YEAR(a.'.$key_date.') = '.$val_key1.') AND ';
                }
                if($val == 'MONTH'){
                    $val_key1 = "'".$DB->ForSql($filter[$key_date.'_month'])."'";
                    $val_key2 = "'".$DB->ForSql($filter[$key_date.'_year'])."'"; 

                    $sql_condition .= '(MONTH(a.'.$key_date.') = '.$val_key1.' AND YEAR(a.'.$key_date.') = '.$val_key2.') AND ';
                }
                if($val == 'RANGE'){ 
                    $start_date = str_replace('/', '-', $filter[$key_date.'_from']);
                    $end_date = str_replace('/', '-', $filter[$key_date.'_to']);
                    
                    $start_date = date('Y-m-d',strtotime($start_date));
                    // $end_date = date('Y-m-d',strtotime($end_date));
                    $end_date = date('Y-m-d',strtotime("-1 day", strtotime($end_date)));

                    $start_date = "'".$DB->ForSql($start_date)."'";
                    $end_date = "'".$DB->ForSql($end_date)."'";
                    
                    $sql_condition .= '(DATE(a.'.$key_date.') BETWEEN '.$start_date.' AND '.$end_date.') AND ';
                }
                break;
        }
    }
}
if(!empty($sql_condition)){
    $sql_condition = substr($sql_condition,0,-5);
    if(count($condition_filter)>1){
        $sql_condition = '('.$sql_condition.')';
    }
}
// CAskarasoft::debug($sql_condition);

if(!empty($filter['FIND'])) {

}
  
// if (!empty($filter['VENDOR_NAME'])) {
//     $filter_text[] = "CONCAT(TIPE_PERUSAHAAN, ' ', NAMA_PERUSAHAAN) LIKE '%".$filter['VENDOR_NAME']."%'";
// }  

// QUERY LIKE FOR EACH DATA EXCEPT DATE DATA
if (!empty($filter['NAMA_KOTA'])) {
    $filter_text[] = "NAMA_KOTA LIKE '%".$filter['NAMA_KOTA']."%'";
}

//QUERY FOR SHOWING DATA
$main_sql =  "
    SELECT 
        ".$DB->DateToCharFunction('a.CREATED_DATE', 'SHORT')." as CR_DATE, 
        ".$DB->DateToCharFunction('a.MODIFIED_DATE', 'SHORT')." as MD_DATE, 
        a.* 
    FROM 
        " . Kota::TABLE_MASTER . " a 
";

if(!$filter_text && empty($sql_condition)) {
    $main_sql = $main_sql;
} else {
    $main_sql = $main_sql." HAVING (";
    if($filter_text){
        $main_sql .= implode(" AND ",$filter_text);
    }
    if(!empty($sql_condition)){
        if($filter_text){
            $main_sql .= " AND ";
        }
        $main_sql .= $sql_condition;
    }
    $main_sql .= ")";
}
// show count data
$res_cnt = $DB->Query("SELECT COUNT(ID) as C FROM (" . $main_sql . " ) bmaster");
$res_cnt = $res_cnt->Fetch();
$totalProducts = (int) $res_cnt["C"];   // unknown by default
// NAV rule
$nav_params = array(
    'nPageSize'          => $navPageSize,
    'bDescPageNumbering' => false,
    'NavShowAll'         => false,
    'bShowAll'           => false,
    'showAlways'         => false,
    'SHOW_ALWAYS'        => false
);

if ($request['type'] == 'excel') {
    $nav_params = array( 
        'nPageSize'          => $totalProducts,
        'bDescPageNumbering' => false,
        'NavShowAll'         => false,
        'bShowAll'           => false,
        'showAlways'         => false,
        'SHOW_ALWAYS'        => false
    );
}

$sort = [];
foreach($aSort['sort'] as $field => $sortMode) $sort[] = "$field $sortMode";

$sort = implode(', ', $sort);
if (!empty($sort)) $main_sql .= ' ORDER BY '.$sort;

$dbRes = new CDBResult();
$rc = $dbRes->NavQuery($main_sql, $totalProducts, $nav_params);

$aRows = array();

// SET EACH ROWS OF DATA FOR SHOWS IT
while( $aRes = $dbRes->fetch() ) { 
    
    // dd($aRes);
    $aCols = array(
        "ID" => $aRes["ID"], 
        "NAMA_KOTA" => $aRes['NAMA_KOTA'],
        "ID_PROVINSI" => $aRes['ID_PROVINSI'],
        "CREATED_DATE" => $aRes["CR_DATE"], 
        "CREATED_BY"   => $aRes["CREATED_BY"],
        "MODIFIED_DATE" => $aRes["MD_DATE"], 
        "MODIFIED_BY"   => $aRes["MODIFIED_BY"],
    );
    $aRows [] = array("data" => $aRes, "actions" => [], "columns" => $aCols, "editable" => false); 
}

// fill navigation parameter and rule for grid
$arResult["NAV_OBJECT"] = $dbRes;
$arResult['NAV_OBJECT']->NavStart($navPageSize, false);
$arResult["ROWS"] = $aRows;

$arResult["ROWS_COUNT"] = $totalProducts;
$arResult["SORT"] = $aSort["sort"];
$arResult["SORT_VARS"] = $aSort["vars"];

$arResult['PAGE_SIZES'] = array(
    array( "NAME" => "10", "VALUE" => "10"),
    array( "NAME" => "20", "VALUE" => "20"),
    array( "NAME" => "50", "VALUE" => "50"),
    array( "NAME" => "100", "VALUE" => "100")
);
 
$arResult['PAGE_FORM'] = $arParams['PAGE_FORM']; 
$arResult['PAGE_DETAIL'] = $arParams['PAGE_DETAIL'];
$arResult['PAGE_LIST'] = $arParams['PAGE_LIST'];

/* DATA FOR HEADER TABLE */
$arResult["ELEMENTS_HEADERS"] = array(
    array(
        "id" => "ID",
        "name" => "ID",
        "default" => false,
        "sort" => "ID"
    ),  
    array(
        "id" => "NAMA_KOTA",
        "name" => "Nama Kota",
        "default" => true,
        "sort" => "NAMA"
    ), 
    array(
        "id" => "ID_PROVINSI",
        "name" => "Nama Provinsi",
        "default" => true,
        "sort" => "NAMA"
    ), 
    array(
        "id" => "CREATED_DATE",
        "name" => "Created Date",
        "default" => true,
        "sort" => "CREATED_DATE"
    ),
    array(
        "id" => "CREATED_BY",
        "name" => "Created By",
        "default" => true,
        "sort" => "CREATED_BY"
    ),
    array(
        "id" => "MODIFIED_DATE",
        "name" => "Modified Date",
        "default" => true,
        "sort" => "MODIFIED_DATE"
    ),
    array(
        "id" => "MODIFIED_BY",
        "name" => "Modified By",
        "default" => true,
        "sort" => "MODIFIED_BY"
    )
);
 
$this->IncludeComponentTemplate(); 