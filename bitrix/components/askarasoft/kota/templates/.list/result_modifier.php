<?php
global $USER, $DB; 

if(!CModule::IncludeModule('askarasoft.provinsi')) {
    ShowError("Module askarasoft.provinsi not installed");
    return;
}
$res = $arResult['ROWS']; 

if (!empty($res)) { 
    
    foreach($res as $i => $row){
        $id = $row['columns']['ID']; 
        
        if(!empty($arResult['PAGE_DETAIL'])){
            $PageDetail = CComponentEngine::makePathFromTemplate(
                $arResult['PAGE_DETAIL'],[ 
                    'ID' => $id,
                ]
            );
        } 
        
        $actions = []; 
        $actions['detail'] = ['TEXT' => 'Detail', 'HREF'=>CUtil::JSEscape($PageDetail), 'DEFAULT'=>true]; 
        // $actions['user'] = ['TEXT' => 'User', 'HREF'=>CUtil::JSEscape($PageUser), 'DEFAULT'=>true]; 

        if(!empty($row['columns']['CREATED_BY'])){
            $data_created_user = CUser::GetByID((int)$row['columns']['CREATED_BY'])->Fetch();
        } 
        if($data_created_user){
            $arResult['ROWS'][$i]['columns']['CREATED_BY'] = $data_created_user['NAME'] . (!empty($data_created_user['LAST_NAME']) ? ' '.$data_created_user['LAST_NAME'] : '');
        }

        if(!empty($row['columns']['MODIFIED_BY'])){
            $data_modified_user = CUser::GetByID((int)$row['columns']['MODIFIED_BY'])->Fetch();
        }
        if($data_modified_user){
            $arResult['ROWS'][$i]['columns']['MODIFIED_BY'] = $data_modified_user['NAME'] . (!empty($data_modified_user['LAST_NAME']) ? ' '.$data_modified_user['LAST_NAME'] : '');
        } 
        if (!empty($row['columns']['ID_PROVINSI'])){
            $data_provinsi = Provinsi::GetById((int)$row['columns']['ID_PROVINSI']);
        }
        if ($data_provinsi){
            $arResult['ROWS'][$i]['columns']['ID_PROVINSI'] = $data_provinsi["NAMA_PROVINSI"];
        }
        // if($row['columns']['STATUS'] === 'draft'){
        //     unset($actions['user']); 
        // }  

        // if (!in_array($USER->GetID(), $explode_user)) { 
        //     unset($actions['user']);  
        // }  

        if($row['columns']['STATUS'] === 'draft'){
            $arResult['ROWS'][$i]['columns']['STATUS'] = 'Draft';
        }   
        
        if(!empty($actions['detail'])){
            $arResult['ROWS'][$i]['actions'][] = $actions['detail'];
        }
        // if(!empty($actions['user'])){
        //     $arResult['ROWS'][$i]['actions'][] = $actions['user'];
        // } 
    }
}
?>