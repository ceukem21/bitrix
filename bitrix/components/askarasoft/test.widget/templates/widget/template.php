<?php
/**
 * Created by PhpStorm.
 * User: A-M
 * Date: 3/4/2021
 * Time: 8:56 AM
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();


$this->SetViewTarget("sidebar", 50);

?>

    <div class="sidebar-widget sidebar-widget-calendar">
    <div class="sidebar-widget-top">
        <div class="sidebar-widget-top-title testing-color">Testing Widget</div>
    </div>
    <div class="sidebar-widget-content">
        <img src="/testing/images/apple_we orib.png"/>
    </div>
    </div>

<script src="/bitrix/js/testscript.js"></script>
<?


$this->EndViewTarget();
?>