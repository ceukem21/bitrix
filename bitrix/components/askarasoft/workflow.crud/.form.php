<?php

use Bitrix\Main\Context;
use Bitrix\Main\Localization\Loc;

if( !defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
// include module from local was named askarasoft.workflow.crud
if(!CModule::IncludeModule('askarasoft.workflow.crud')) {
    ShowError("Module askarasoft.workflow.crud not installed");
    return;
}

global $DB; 

$arResult['PAGE_FORM'] = $arParams['PAGE_FORM']; 
$arResult['PAGE_DETAIL'] = $arParams['PAGE_DETAIL'];
$arResult['PAGE_LIST'] = $arParams['PAGE_LIST'];

if ($_SERVER['REQUEST_METHOD'] == 'GET') { 
    // $action = $_GET['action'];

    // if(isset($action)){  
    //     if($action === 'change_po'){              
    //         $APPLICATION->RestartBuffer();
    //         Header('Content-Type: application/x-javascript; charset='.LANG_CHARSET); 
    //         $search = $DB->ForSql($_GET['search']);
            
    //         $get_po = KinerjaVendor::searchPO(array(
    //             'PO_NUMBER'=>$search
    //         )); 

    //         echo \Bitrix\Main\Web\Json::encode(array(
    //             'items'=>$get_po, 
    //         ));   
    //         exit; 
    //     } 
    // }    
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') { 
    $APPLICATION->RestartBuffer();
    
    if (isset($_POST['process']) && $_POST['process'] == 'true') {
        // check CSRF form
        if (check_bitrix_sessid()) {
            
            foreach ($_POST as $k => $v) {
                if($k == 'action'){
                    continue;
                }
                    // FOR CK EDITOR DOESNT NEED FUNCTION ForSql()
                if($k == 'input_deskripsi'){
                    $arData[strtoupper($k)] = $v; 
                }else{
                    $arData[strtoupper($k)] = $DB->ForSql($v);
                }
            }

            $res_id = WorkflowCRUD::save($arData);

            // res_id result = ID CREATED
            if(!$res_id){
                // CAskarasoft::alert('Error','Failed to save:(1)','error');
                // LocalRedirect("/test_crud/form.php"); 
                echo '<pre>';
                var_dump([
                    'Error',
                    'Failed to save:(1)',
                ]);
                echo '</pre>';
                exit;
            }
            // CAskarasoft::alert('Success','Saved','success');
            LocalRedirect("/bizproc/processes/18/view/0/?list_section_id="); 
        } else {
            showError('form tidak valid');
        }
    } 
}  

$this->IncludeComponentTemplate($template); 