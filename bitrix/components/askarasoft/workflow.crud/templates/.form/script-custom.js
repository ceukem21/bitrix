 
function formValidate() { 
    showLoading(true);
    return true;
} 

$(document).ready(function () { 
   
}); 

const showLoading = (is_open = true, txt_icon = 'info', txt_title = 'Wait ...', txt_html = '', allowclick = false) => {
    if (is_open) {
        Swal.close();
        Swal.fire({
            title: txt_title,
            html: txt_html,
            icon: txt_icon,
            iconHtml: '<i class="fa fa-info"></i>',
            allowOutsideClick: allowclick,
            allowEscapeKey: allowclick,
            allowEnterKey: allowclick,
            showConfirmButton: allowclick,
            showCancelButton: false,
            showCloseButton: false,
            // timer: 1500
        });
    } else {
        Swal.close();
    }
}
 