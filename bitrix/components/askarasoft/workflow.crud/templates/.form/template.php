<?php
use Bitrix\Main\Localization\Loc;
\Bitrix\Main\UI\Extension::load("ui.buttons");
\Bitrix\Main\UI\Extension::load("ui.alerts");
\Bitrix\Main\UI\Extension::load("ui.bootstrap4");
$APPLICATION->setTitle('Form WorkFlow');

$APPLICATION->SetAdditionalCSS("/bitrix/components/askarasoft/workflow.crud/templates/.assets/font-awesome-4.7.0/css/font-awesome.min.css");  
$APPLICATION->AddHeadScript("/bitrix/components/askarasoft/workflow.crud/templates/.assets/sweetalert2.js");

$ASSET = \Bitrix\Main\Page\Asset::getInstance();
$ASSET->addJs('/bitrix/js/crm/common.js');
// $ASSET->addJs("/bitrix/components/askarasoft/workflow.crud/templates/.assets/datatables.js"); 

$this->SetViewTarget('pagetitle', 100);

if (isset($_SERVER['HTTPS']) &&
    ($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1) ||
    isset($_SERVER['HTTP_X_FORWARDED_PROTO']) &&
    $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') {
    $protocol = 'https://';
}
else {
    $protocol = 'http://';
} 

$PageList = CComponentEngine::makePathFromTemplate(
    $arResult['PAGE_LIST']
); 

?> 

<!-- <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet"/>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script> -->

<div class="pagetitle-container pagetitle-align-right-container">
    <a href="<?= CUtil::JSEscape($PageList); ?>" class="webform-small-button webform-small-button-transparent">
        <i style="font-size: 15px;" class="fa fa-angle-left"></i>
        <span class="webform-small-button-text">Back to List</span>
    </a>
</div>
<?
$this->EndViewTarget(); 
// CAskarasoft::show_alert();
?> 
<!-- <script type="text/javascript" src="/bitrix/components/askarasoft/askarasoft.assets/templates/.default/ckeditor-4.16.1/ckeditor.js"></script> -->

<div class="container-fluid"> 
    <div class="card">
        <div class="card-body">
            <form onSubmit="return formValidate();" action="" method="POST" enctype="multipart/form-data" id="form_crud"> 
                <?= bitrix_sessid_post(); ?>
                <input type="hidden" name="process" value="true"/>
                <table>
                    <tr>
                        <td>ID</td>
                        <td><input name="id"/></td>
                    </tr>
                    <tr>
                        <td>Nama Pegawai</td>
                        <td><input name="nama_pegawai"/></td>
                    </tr>
                    <tr>
                        <td>Waktu Lembur</td>
                        <td><input name="waktu_lembur" /></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><input type="submit" name="btn_submit" value="Save"/></td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</div> 

<!-- -------------------------------------- -->
 
<div class="modal" id="alert__modal" data-backdrop="static" data-keyboard="false"></div>

<script>
    var current_loc = "/bitrix/components/askarasoft/workflow.crud/templates/.form/";
    var protocol = '<?= $protocol; ?>';
    var server_name = '<?= $_SERVER['SERVER_NAME']; ?>'; 

    var script = document.createElement("script");
    script.src = "/bitrix/components/askarasoft/workflow.crud/templates/.form/script-custom.js";
    document.head.appendChild(script);
</script>