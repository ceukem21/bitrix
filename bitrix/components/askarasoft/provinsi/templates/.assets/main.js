function escapeOutput(toOutput){
    return toOutput.replace(/\&/g, '&amp;')
        .replace(/\</g, '&lt;')
        .replace(/\>/g, '&gt;')
        .replace(/\"/g, '&quot;')
        .replace(/\'/g, '&#x27')
        .replace(/\//g, '&#x2F');
}

function setInputFilter(textbox, inputFilter, callback=false, off_first_event=false, remove_event=false) {
    let array_event = [
        "input", "keydown", "keyup", 
        "mousedown", "mouseup", "select", 
        "contextmenu", "drop"
    ];

	if(remove_event !== false){
		array_event = array_event.filter( function( el ) {
			return remove_event.indexOf( el ) < 0;
		});
	}

    if(off_first_event == true){
        array_event.forEach(function(event) {
            textbox.off(event);
        });
    }

    array_event.forEach(function(event) {
        textbox.on(event, function() {
			if (inputFilter(this.value)) {
				this.oldValue = this.value;
				this.oldSelectionStart = this.selectionStart;
				this.oldSelectionEnd = this.selectionEnd;
			} else if (this.hasOwnProperty("oldValue")) {
				this.value = this.oldValue;
				this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
				// handler when cannot to continue
				if(callback != false){
					callback(textbox);
				}
			} else {
				this.value = "";
			}
		});
	});
	// Integer
	// setInputFilter(this_element, function(value) {
	//   return /^-?\d*$/.test(value); });

	// Integer >= 0
	// setInputFilter(this_element, function(value) {
	//   return /^\d*$/.test(value); });

	// Integer >= 0 and <= 500
	// setInputFilter(this_element, function(value) {
	//   return /^\d*$/.test(value) && (value === "" || parseInt(value) <= 500); });

	// Float (use . or , as decimal separator)
	// setInputFilter(this_element, function(value) {
	//   return /^-?\d*[.,]?\d*$/.test(value); });

	// Currency (at most two decimal places)	
	// setInputFilter(this_element, function(value) {
	//   return /^-?\d*[.,]?\d{0,2}$/.test(value); });

	// A-Z only
	// setInputFilter(this_element, function(value) {
	//   return /^[a-z]*$/i.test(value); });

	// Hexadecimal
	// setInputFilter(this_element, function(value) {
	//   return /^[0-9a-f]*$/i.test(value); });
}