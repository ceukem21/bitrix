<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

if($arParams['AJAX'] !== "YES"){
    // echo "<script>show_('bpd');</script>";
}

date_default_timezone_set("Asia/Jakarta");

// global $USER;

$destination = ($arParams['DESTINATION']) ? $arParams['DESTINATION'] : '.index';

$allowed = 1;
if ($allowed == 0) {
	ShowError('Not Allowed');
	die();
}

include_once("." . $destination . ".php");