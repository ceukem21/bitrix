<?php

use Bitrix\Main\Context;
use Bitrix\Main\Localization\Loc;

if( !defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

if(!CModule::IncludeModule('askarasoft.test.crud')) {
    ShowError("Module askarasoft.test.crud not installed");
    return;
}

global $DB, $USER;

$arResult['PAGE_FORM'] = $arParams['PAGE_FORM']; 
$arResult['PAGE_DETAIL'] = $arParams['PAGE_DETAIL'];
$arResult['PAGE_LIST'] = $arParams['PAGE_LIST'];

$id_user = $USER->GetID(); // id user login
$request = Context::getCurrent()->getRequest();
$id = (int) $request['id']; // id detail
$arResult['id'] = $DB->ForSql($id);  

if(empty($arResult['id'])){
    CAskarasoft::alert('Error','ID CRUD is required','error');
    LocalRedirect("/test_crud"); 
}  

$arResult['DATA'] = TestCRUD::GetByID($arResult['id']); 

if ($_SERVER['REQUEST_METHOD'] == 'GET') { 
    // $action = $_GET['action'];

    // if(isset($action)){  
    //     if($action === 'change_po'){              
    //         $APPLICATION->RestartBuffer();
    //         Header('Content-Type: application/x-javascript; charset='.LANG_CHARSET); 
    //         $search = $DB->ForSql($_GET['search']);
            
    //         $get_po = KinerjaVendor::searchPO(array(
    //             'PO_NUMBER'=>$search
    //         )); 

    //         echo \Bitrix\Main\Web\Json::encode(array(
    //             'items'=>$get_po, 
    //         ));   
    //         exit; 
    //     } 
    // }
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') { 
    $APPLICATION->RestartBuffer();
    
    if (isset($_POST['process']) && $_POST['process'] == 'true') {
        if (check_bitrix_sessid()) {
            
            foreach ($_POST as $k => $v) {
                if($k == 'action'){
                    continue;
                }

                if($k == 'input_deskripsi'){
                    $arData[strtoupper($k)] = $v; 
                }else{
                    $arData[strtoupper($k)] = $DB->ForSql($v);
                }
            }
 
            $res_id = TestCRUD::update($arData);
            if(!$res_id){
                // CAskarasoft::alert('Error','Failed to save:(1)','error');
                // LocalRedirect("/test_crud/detail.php?id=".$res_id); 
                echo '<pre>';
                var_dump([
                    'Error',
                    'Failed to save:(1)',
                ]);
                echo '</pre>';
                exit;
            }
            // CAskarasoft::alert('Success','Saved','success');
            LocalRedirect("/test_crud/detail.php?id=".$res_id); 
        } else {
            showError('form tidak valid');
        }
    } 
}  

$this->IncludeComponentTemplate($template); 