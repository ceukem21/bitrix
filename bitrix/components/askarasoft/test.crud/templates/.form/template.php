<?php
use Bitrix\Main\Localization\Loc;
\Bitrix\Main\UI\Extension::load("ui.buttons");
\Bitrix\Main\UI\Extension::load("ui.alerts");
\Bitrix\Main\UI\Extension::load("ui.bootstrap4");
$APPLICATION->setTitle('Form CRUD');

$APPLICATION->SetAdditionalCSS("/bitrix/components/askarasoft/test.crud/templates/.assets/font-awesome-4.7.0/css/font-awesome.min.css");  
$APPLICATION->AddHeadScript("/bitrix/components/askarasoft/test.crud/templates/.assets/sweetalert2.js");

$ASSET = \Bitrix\Main\Page\Asset::getInstance();
$ASSET->addJs('/bitrix/js/crm/common.js');
// $ASSET->addJs("/bitrix/components/askarasoft/test.crud/templates/.assets/datatables.js"); 

$this->SetViewTarget('pagetitle', 100);

if (isset($_SERVER['HTTPS']) &&
    ($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1) ||
    isset($_SERVER['HTTP_X_FORWARDED_PROTO']) &&
    $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') {
    $protocol = 'https://';
}
else {
    $protocol = 'http://';
} 

$PageList = CComponentEngine::makePathFromTemplate(
    $arResult['PAGE_LIST']
); 

?> 

<!-- <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet"/>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script> -->

<div class="pagetitle-container pagetitle-align-right-container">
    <a href="<?= CUtil::JSEscape($PageList); ?>" class="webform-small-button webform-small-button-transparent">
        <i style="font-size: 15px;" class="fa fa-angle-left"></i>
        <span class="webform-small-button-text">Back to List</span>
    </a>
</div>
<?
$this->EndViewTarget(); 
// CAskarasoft::show_alert();
?> 
<!-- <script type="text/javascript" src="/bitrix/components/askarasoft/askarasoft.assets/templates/.default/ckeditor-4.16.1/ckeditor.js"></script> -->

<div class="container-fluid"> 
    <div class="card">
        <div class="card-body">
            <form onSubmit="return formValidate();" action="" method="POST" enctype="multipart/form-data" id="form_crud"> 
                <?= bitrix_sessid_post(); ?>
                <input type="hidden" name="process" value="true"/>
                <table>
                    <tr>
                        <td>Nama</td>
                        <td><input name="nama"/></td>
                    </tr>
                    <tr>
                        <td>Alamat</td>
                        <td><input name="alamat" /></td>
                    </tr>
                    <tr>
                        <td>Telepon</td>
                        <td><input name="telepon"/></td>
                    </tr>
                    <tr>
                        <td>Provinsi</td>
                        <td>
                            <select name="id_provinsi" id="provinsi">
                                <?php 
                                $data_provinsi = $arResult['DATA_PROVINSI'];
                                for ($i = 0;$i < $arResult['DATA_TOTAL']; $i++) {
                                    if ($arResult['DATA_TOTAL'] > 1){
                                ?>
                                        <option value="<?=$data_provinsi[$i]['ID']?>"><?=$data_provinsi[$i]['NAMA_PROVINSI']?></option> 
                                <?php
                                    }
                                    else {
                                ?>
                                        <option value="<?=$data_provinsi['ID']?>"><?=$data_provinsi['NAMA_PROVINSI']?></option>
                                <?php  
                                    }
                                }
                                ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>Kota</td>
                        <td>
                            <select name="id_kota" id="kota">
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><input type="submit" name="btn_submit" value="Save"/></td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</div> 

<!-- -------------------------------------- -->
 
<div class="modal" id="alert__modal" data-backdrop="static" data-keyboard="false"></div>

<script>
    var current_loc = "/bitrix/components/askarasoft/test.crud/templates/.form/";
    var protocol = '<?= $protocol; ?>';
    var server_name = '<?= $_SERVER['SERVER_NAME']; ?>'; 

    var script = document.createElement("script");
    script.src = "/bitrix/components/askarasoft/test.crud/templates/.form/script-custom.js";
    document.head.appendChild(script);
</script>
<script>
    $(document).ready(function() {
        $('#provinsi').on('change', function() {
            var provinsi_id = this.value;
            $.ajax({
                url: "http://"+server_name+"/kota/get-kota.php",
                type: "POST",
                data: {
                    provinsi: provinsi_id
                },
                cache: false,
                success: function(result){
                    $("#kota").html(result);
                    console.log(result);
                }
            });
        });
    });
</script>