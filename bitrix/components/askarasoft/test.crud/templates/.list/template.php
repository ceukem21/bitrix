<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

use Bitrix\Main\Localization\Loc;

global $APPLICATION;
Loc::loadMessages(__FILE__);

$APPLICATION->setTitle('Evaluasi Kinerja Vendor');

$bodyClass = $APPLICATION->GetPageProperty("BodyClass");
$APPLICATION->SetPageProperty("BodyClass", ($bodyClass ? $bodyClass." " : "")."page-one-column pagetitle-toolbar-field-view tasks-pagetitle-view"); 
$showQuickForm = false;

$APPLICATION->SetAdditionalCSS("/local/components/eprocurement/purchase.requisition/templates/.assets/font-awesome-4.7.0/css/font-awesome.min.css"); 
// $APPLICATION->SetAdditionalCSS("/local/components/eprocurement/purchase.requisition/templates/.assets/datatables.css"); 
$APPLICATION->AddHeadScript("/local/components/eprocurement/kinerja_vendor/templates/.assets/script_filter_user.js");
$APPLICATION->AddHeadScript("/local/components/eprocurement/purchase.requisition/templates/.assets/sweetalert2.js");

$ASSET = \Bitrix\Main\Page\Asset::getInstance();
// $ASSET->addJs("/local/components/eprocurement/purchase.requisition/templates/.assets/datatables.js");
?>

<? $this->SetViewTarget("below_pagetitle",1); ?>
<div class="calendar-view-switcher pagetitle-align-right-container">
    <div class="calendar-view-switcher-list-item">
        <ul class="askarasoft-sub-menu" style="padding: 0;margin-top: 0;margin-bottom: 0;">
            <li class="askarasoft-sub-menu-list">
                <div class="askarasoft-sub-menu-title-active">
                    <a href="<?= "./index.php"; ?>">List</a>
                </div>
            </li>
            <li class="askarasoft-sub-menu-list">
                <div class="askarasoft-sub-menu-title"> 
                <a href="<?= "./kanban.php"; ?>">Kanban</a>
                </div>
            </li>
        </ul>
    </div>
</div>
<? $this->EndViewTarget(); ?>

<?php
// CAskarasoft::show_alert();
$this->setViewTarget('inside_pagetitle');
$APPLICATION->IncludeComponent(
    'bitrix:main.ui.filter',
    '',
    array(
        'GRID_ID' => $arResult ["GRID_ID"],
        'FILTER_ID' => $arResult ["GRID_ID"],
        'FILTER' => $arResult ['FILTER'],
        'ENABLE_LIVE_SEARCH' => true,
        'DISABLE_SEARCH' => true,
        'ENABLE_LABEL' => true
    ),
    $component,
    array("HIDE_ICONS" => true)
);
$this->endViewTarget(); 

$this->SetViewTarget('inside_pagetitle'); ?>
<div class="pagetitle-container pagetitle-align-right-container">
    <span class="webform-small-button-wrap">
        <a href="form.php" class="webform-small-button webform-small-button-blue tippy_title" data-tippy-content="Buat baru Form Kinerja Vendor"
           id="tasks-buttonAdd">
            <i class="fa fa-plus"></i>&nbsp;&nbsp;NEW
        </a>
    </span>
</div>
<? $this->EndViewTarget(); ?>

<?php
$APPLICATION->IncludeComponent(
    'bitrix:main.ui.grid',
    '',
    array(
        'GRID_ID'   => $arResult['GRID_ID'],
        'HEADERS'   => isset($arResult['HEADERS']) ? $arResult['HEADERS'] : array(),
        "COLUMNS"   => $arResult["ELEMENTS_HEADERS"],
        'SORT'      => isset($arResult['SORT']) ? $arResult['SORT'] : array(),
        'SORT_VARS' => isset($arResult['SORT_VARS']) ? $arResult['SORT_VARS'] : array(),
        'ROWS'      => $arResult['ROWS'],

        'AJAX_MODE'           => 'Y',
        //Strongly required
        "AJAX_OPTION_JUMP"    => "N",
        "AJAX_OPTION_STYLE"   => "N",
        "AJAX_OPTION_HISTORY" => "N",

        "ALLOW_COLUMNS_SORT"      => true,
        "ALLOW_ROWS_SORT"         => false,
        "ALLOW_COLUMNS_RESIZE"    => true,
        "ALLOW_HORIZONTAL_SCROLL" => true,
        "ALLOW_SORT"              => true,
        "ALLOW_PIN_HEADER"        => true,
        "ACTION_PANEL"            => [],

        "SHOW_CHECK_ALL_CHECKBOXES" => false,
        "SHOW_ROW_CHECKBOXES"       => false,
        "SHOW_ROW_ACTIONS_MENU"     => true,
        "SHOW_GRID_SETTINGS_MENU"   => true,
        "SHOW_NAVIGATION_PANEL"     => true,
        "SHOW_PAGINATION"           => true,
        "SHOW_SELECTED_COUNTER"     => false,
        "SHOW_TOTAL_COUNTER"        => true,
        "SHOW_PAGESIZE"             => true,
        "SHOW_ACTION_PANEL"         => false,

        "MESSAGES" => '',

        "ENABLE_COLLAPSIBLE_ROWS" => true,

        "SHOW_MORE_BUTTON" => false,
        '~NAV_PARAMS'       => $arResult['GET_LIST_PARAMS']['NAV_PARAMS'],
        'NAV_OBJECT'       => $arResult['NAV_OBJECT'],
        'NAV_STRING'       => $arResult['NAV_STRING'],

        "TOTAL_ROWS_COUNT"  => $arResult['ROWS_COUNT'],
        "PAGE_SIZES"        => $arResult['PAGE_SIZES'],
        "DEFAULT_PAGE_SIZE" => 10
    ),
    $component,
    array('HIDE_ICONS' => 'Y')
);

\CUtil::initJSCore(
    array(
        'tasks_integration_socialnetwork'
    )
);
$selectorID = 'user'; 
$selectorMode = 'user';
$multi = true;

?>

<div class="modal" id="action__modal" ></div>
<div class="modal" id="alert__modal" data-backdrop="static" data-keyboard="false"></div>


<script>
    var self_data_user = <?= json_encode($arParams['self_data_user']); ?>;
    
    var script = document.createElement("script");
    script.src = "/bitrix/components/askarasoft/test.crud/templates/.list/script-custom.js";
    document.head.appendChild(script);
    
    BX.ready(
        function() {
            BX.FilterEntitySelector.create(
                "<?= \CUtil::JSEscape($selectorID)?>",
                {
                    fieldId: "<?= \CUtil::JSEscape('USER_REVIEW')?>",
                    mode: "<?= \CUtil::JSEscape($selectorMode)?>",
                    multi: <?= $multi ? 'true' : 'false'?>
                }
            );
        }
    ); 
    
</script>