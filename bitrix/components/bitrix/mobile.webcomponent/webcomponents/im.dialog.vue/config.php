<?
return [
	"rel" => [
		"mobile.im.application.dialog",
		"mobile_ui",
		"mobile_tools",
		"mobile_uploader",
	],
	"deps" => [
		"menu/backdrop",
		"menu/header",

		"webcomponent/parameters",
		"utils/urlrewrite/rules",
		"webcomponent/urlrewrite",
		"webcomponent/storage",

		"chat/widgetcache",
		"chat/const/background",
		"chat/performance",
	],
	"js" => [],
	"css"=>[],
	"images"=>[
		"/bitrix/js/ui/icons/disk/images/"
	],
	"langs" => [],
	"exclude" => []
];