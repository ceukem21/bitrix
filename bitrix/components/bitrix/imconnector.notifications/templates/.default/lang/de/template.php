<?php
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_CONNECTED"] = "Bitrix24 Benachrichtigungscenter ist verbunden";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_HEADER"] = "SMS und WhatsApp Nachrichten in Bitrix24";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_INDEX_DESCRIPTION"] = "Verbinden Sie das Bitrix24 Benachrichtigungscenter mit einem Kommunikationskanal, um mobile Benachrichtigungen an Ihren Kunden zu senden";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_INDEX_DESCRIPTION_1"] = "Vorlagenbasierte Nachrichten können mit dem Onlineshop und der Aktion des Zahlungsempfangs im Auftragsprofil genutzt werden.";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_INDEX_DETAILS"] = "Details";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_SIMPLE_FORM_DESCRIPTION_1"] = "Der Kommunikationskanal Ihres Unternehmens ist mit dem Bitrix24 Benachrichtigungscenter bereits verbunden";
