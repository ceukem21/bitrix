<?php
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_CONNECTED"] = "Bitrix24 Notification Center is connected";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_HEADER"] = "SMS and WhatsApp messages in Bitrix24";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_INDEX_DESCRIPTION"] = "Connect Bitrix24 Notification Center to Open Channel to send mobile notifications to your customers";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_INDEX_DESCRIPTION_1"] = "Template based messages can be used with the new Online Store and the Receive Payment action in the deal form.";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_INDEX_DETAILS"] = "Details";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_SIMPLE_FORM_DESCRIPTION_1"] = "Your company's Open Channel is already connected to Bitrix24 Notification Center";
