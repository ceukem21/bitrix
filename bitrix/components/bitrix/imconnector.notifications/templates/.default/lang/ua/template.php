<?php
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_CONNECTED"] = "Центр сповіщень Бітрікс24 підключений";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_HEADER"] = "СМС і WhatsApp повідомлення Бітрікс24";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_INDEX_DESCRIPTION"] = "Підключіть Центр сповіщень Бітрікс24 до Відкритої лінії, щоб відправляти сповіщення на мобільні телефони ваших клієнтів";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_INDEX_DESCRIPTION_1"] = "Сценарій відправки повідомлень за шаблоном працює для нового «Інтернет-магазину» і «Прийняти оплату» в картці угоди.";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_INDEX_DETAILS"] = "Детальніше";
$MESS["IMCONNECTOR_COMPONENT_NOTIFICATIONS_SIMPLE_FORM_DESCRIPTION_1"] = "Відкрита лінія вашої компанії вже підключена до Центру сповіщень Бітрікс24";
