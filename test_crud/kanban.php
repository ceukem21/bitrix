<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
//show the crm type popup (with or without leads)
if (!\Bitrix\Crm\Settings\LeadSettings::isEnabled())
{
	CCrmComponentHelper::RegisterScriptLink('/bitrix/js/crm/common.js');
	?><script><?=\Bitrix\Crm\Settings\LeadSettings::showCrmTypePopup();?></script><?
}
// js/css
$APPLICATION->SetAdditionalCSS('/bitrix/themes/.default/bitrix24/crm-entity-show.css');
$bodyClass = $APPLICATION->GetPageProperty('BodyClass');
$APPLICATION->SetPageProperty('BodyClass', ($bodyClass ? $bodyClass.' ' : '').'no-paddings grid-mode pagetitle-toolbar-field-view crm-toolbar');
$asset = Bitrix\Main\Page\Asset::getInstance();
$asset->addJs('/bitrix/js/crm/common.js');

// some common langs
use Bitrix\Main\Localization\Loc;
Loc::loadMessages($_SERVER['DOCUMENT_ROOT'].'/bitrix/components/bitrix/crm.lead.menu/component.php');
Loc::loadMessages($_SERVER['DOCUMENT_ROOT'].'/bitrix/components/bitrix/crm.lead.list/templates/.default/template.php');

\Bitrix\Crm\Settings\Crm::markAsInitiated();

// if not isset
$arResult['PATH_TO_LEAD_EDIT'] = isset($arResult['PATH_TO_LEAD_EDIT']) ? $arResult['PATH_TO_LEAD_EDIT'] : '';
$arResult['PATH_TO_LEAD_LIST'] = isset($arResult['PATH_TO_LEAD_LIST']) ? $arResult['PATH_TO_LEAD_LIST'] : '';
$arResult['PATH_TO_LEAD_WIDGET'] = isset($arResult['PATH_TO_LEAD_WIDGET']) ? $arResult['PATH_TO_LEAD_WIDGET'] : '';
$arResult['PATH_TO_LEAD_KANBAN'] = isset($arResult['PATH_TO_LEAD_KANBAN']) ? $arResult['PATH_TO_LEAD_KANBAN'] : '';
$arResult['PATH_TO_LEAD_CALENDAR'] = isset($arResult['PATH_TO_LEAD_CALENDAR']) ? $arResult['PATH_TO_LEAD_CALENDAR'] : '';
$arResult['PATH_TO_LEAD_DEDUPE'] = isset($arResult['PATH_TO_LEAD_DEDUPE']) ? $arResult['PATH_TO_LEAD_DEDUPE'] : '';
$arResult['PATH_TO_LEAD_IMPORT'] = isset($arResult['PATH_TO_LEAD_IMPORT']) ? $arResult['PATH_TO_LEAD_IMPORT'] : '';

$entityType = \CCrmOwnerType::LeadName;

// start ambil data dr database test CRUD
if(!CModule::IncludeModule('askarasoft.test.crud')) {
    ShowError("Module askarasoft.test.crud not installed");
    return;
}
global $USER, $DB;
$self_user_id = $USER->GetID();
$self_data_user = CUser::GetByID($self_user_id)->Fetch();

$request = \Bitrix\Main\Context::getCurrent()->getRequest();

$main_sql =  "
    SELECT 
        ".$DB->DateToCharFunction('a.CREATED_DATE', 'SHORT')." as CR_DATE, 
        ".$DB->DateToCharFunction('a.MODIFIED_DATE', 'SHORT')." as MD_DATE, 
        a.* 
    FROM 
        " . TestCRUD::TABLE_MASTER . " a 
	ORDER BY 
		ID DESC
";

$res_cnt = $DB->Query("SELECT COUNT(ID) as C FROM (" . $main_sql . " ) bmaster");
$res_cnt = $res_cnt->Fetch();
$totalProducts = (int) $res_cnt["C"];   // unknown by default
$navPageSize = 999;
$nav_params = array(
    'nPageSize'          => $navPageSize,
    'bDescPageNumbering' => false,
    'NavShowAll'         => false,
    'bShowAll'           => false,
    'showAlways'         => false,
    'SHOW_ALWAYS'        => false
);

$dbRes = new CDBResult();
$rc = $dbRes->NavQuery($main_sql, $totalProducts, $nav_params);
$aRows = array();
while( $aRes = $dbRes->fetch() ) { 
        $fields [] = array(
            "code" => "TITLE",
            "title" => "Name",
            "type"  => "string",
            "value" => $aRes['NAMA'],
            "valueDelimiter" => "",
            "icon" => "",
            "html" => "",
        );
        $fields [] = array(
            "code" => "OPPORTUNITY",
            "title" => "Total",
            "type"  => "double",
            "value" => "$0",
            "valueDelimiter" => "",
            "icon" => "",
            "html" => "",
        );
        $fields [] = array(
            "code" => "DATE_CREATE",
            "title" => "",
            "type"  => "string",
            "value" => $aRes["CR_DATE"],
            "valueDelimiter" => "",
            "icon" => "",
            "html" => "",
        );
    // dd($aRes);
        $rf = array(
            "EMAIL" => "",
            "PHONE" => "",
            "WEB" => ""
        );
        $email = array(
            "contact" => array(
                            "value" => "cat@aksicorp.com",
                            "title" => "Work"
                        ),
            "company" => array(
                            "value" => "info@aksicorp.com",
                            "title" => "Work"
                       )
        );
        $phone = array(
            "contact" => array(
                            "value" => $aRes['TELEPON'],
                            "title" => "Work Phone"
                        ),
            "company" => array(
                            "value" => $aRes['TELEPON'],
                            "title" => "Work Phone"
                        )
        );
        $web = array(
            "contact" => array(
                            "value" => "www.aksicorp.com",
                            "title" => "Corporate"
                        ),
            "company" => array(
                            "value" => "www.aksicorp.com",
                            "title" => "Corporate"
                       )
        );
    $aCols = array(
        "id" => $aRes["ID"], 
        "name" => $aRes['NAMA'],
        "link" => "test_crud/detail.php?id=".$aRes["ID"],
        "columnId" => "NEW",
        "columnColor" => "39A8EF",
        "price" => 0,
        "date" => "",
        "contactId" => 7,
        "companyId" => 1,
        "contactType" => "CRM_COMPANY",
        "modifyById" => 1,
        "modifyByAvatar" => "",
        "activityShow" => 1,
        "activityErrorTotal" => 0,
        "activityProgress" => 0,
        "activityTotal" => 0,
        "page" => 1,
        "pageCount" => 1,
        "price_formatted" => "$0",
        "fields" => $fields,
        "return" => 1,
        "returnApproach" => "",
        "assignedBy" => 1,
        "required" => array(),
        "required_fm" => $rf,
        "contactName" => "Catherine Langford",
        "contactTooltip" => "",
        "companyName" => "AKSI Corporation",
        "companyTooltip" => "",
        "email" => $email,
        "phone" => $phone,
        "web" => $web
    );
    $aRows [] = array(
                    "id" => $aRes["ID"],
                    "collumnId" => "NEW",
                    "countable" => 1,
                    "droppable" => 1,
                    "draggable" => 1,
                    "data" => $aCols, 
                    ); 
}
$arResult["ROWS"] = $aRows;
// echo $totalProducts;
// stop
// echo "<pre>";
// print_r($arResult["ROWS"]);
// echo "</pre>";
\Bitrix\Tasks\Ui\Filter\Task::setUserId($arResult[ "VARIABLES" ][ "user_id" ]);
	$state = \Bitrix\Tasks\Ui\Filter\Task::listStateInit()->getState();

	$componentName = 'bitrix:tasks.kanban';

	$APPLICATION->IncludeComponent(
		$componentName,
		".default",
		Array(
			"INCLUDE_INTERFACE_HEADER" => "Y",
			"PERSONAL" => "N",
			"KANBAN_USE_MENU_AJAX_ROLE_FILTER"=>'N',
			"USER_ID" => $arResult["VARIABLES"]["user_id"],
			"STATE" => array(
				'ROLES'=>$state['ROLES'],
				'SELECTED_ROLES'=>$state['ROLES'],
				'VIEWS'=>$state['VIEWS'],
				'SELECTED_VIEWS'=>$state['VIEWS'],
			),
			"ITEMS_COUNT" => "50",
			"PAGE_VAR" => $arResult["ALIASES"]["page"],
			"USER_VAR" => $arResult["ALIASES"]["user_id"],
			"VIEW_VAR" => $arResult["ALIASES"]["view_id"],
			"TASK_VAR" => $arResult["ALIASES"]["task_id"],
			"ACTION_VAR" => $arResult["ALIASES"]["action"],
			"PATH_TO_USER_PROFILE" => $arResult["PATH_TO_USER"],
			"PATH_TO_MESSAGES_CHAT" => $arResult["PATH_TO_MESSAGES_CHAT"],
			"PATH_TO_CONPANY_DEPARTMENT" => $arParams["PATH_TO_CONPANY_DEPARTMENT"],
			"PATH_TO_VIDEO_CALL" => $arResult["PATH_TO_VIDEO_CALL"],
			"PATH_TO_USER_TASKS" => $arResult["PATH_TO_USER_TASKS"],
			"PATH_TO_USER_TASKS_BOARD" => $arResult["PATH_TO_USER_TASKS_BOARD"],
			"PATH_TO_USER_TASKS_TASK" => $arResult["PATH_TO_USER_TASKS_TASK"],
			"PATH_TO_USER_TASKS_VIEW" => $arResult["PATH_TO_USER_TASKS_VIEW"],
			"PATH_TO_USER_TASKS_REPORT" => $arResult["PATH_TO_USER_TASKS_REPORT"],
			"PATH_TO_USER_TASKS_TEMPLATES" => $arResult["PATH_TO_USER_TASKS_TEMPLATES"],
			"PATH_TO_GROUP" => $arParams["PATH_TO_GROUP"],
			"PATH_TO_GROUP_TASKS" => $arParams["PATH_TO_GROUP_TASKS"],
			"PATH_TO_GROUP_TASKS_BOARD" => $arParams["PATH_TO_GROUP_TASKS_BOARD"],
			"PATH_TO_GROUP_TASKS_TASK" => $arParams["PATH_TO_GROUP_TASKS_TASK"],
			"PATH_TO_GROUP_TASKS_VIEW" => $arParams["PATH_TO_GROUP_TASKS_VIEW"],
			"PATH_TO_GROUP_TASKS_REPORT" => $arParams["PATH_TO_GROUP_TASKS_REPORT"],
			'PATH_TO_USER_TASKS_PROJECTS_OVERVIEW' => $arResult['PATH_TO_USER_TASKS_PROJECTS_OVERVIEW'],
			"SET_NAV_CHAIN" => $arResult["SET_NAV_CHAIN"],
			"SET_TITLE" => $arResult["SET_TITLE"],
			"FORUM_ID" => $arParams["TASK_FORUM_ID"],
			"NAME_TEMPLATE" => $arParams["NAME_TEMPLATE"],
			"SHOW_LOGIN" => $arParams["SHOW_LOGIN"],
			"DATE_TIME_FORMAT" => $arResult["DATE_TIME_FORMAT"],
			"SHOW_YEAR" => $arParams["SHOW_YEAR"],
			"CACHE_TYPE" => $arParams["CACHE_TYPE"],
			"CACHE_TIME" => $arParams["CACHE_TIME"],
			"USE_THUMBNAIL_LIST" => "N",
			"INLINE" => "Y",
			"USE_PAGINATION"=>'Y',
			'HIDE_OWNER_IN_TITLE' => $arParams['HIDE_OWNER_IN_TITLE'],
			"PREORDER" => array('STATUS_COMPLETE' => 'asc')
		),
		$component,
		array("HIDE_ICONS" => "Y")
	);


require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
