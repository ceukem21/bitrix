<?
if (! \Bitrix\Main\Loader::includeModule('rest') || ! CModule::includeModule('im') || ! CModule::includeModule('askarasoft.module') || !CModule::IncludeModule('askarasoft.workflow')) {
    return;
}

use Bitrix\Main;
use Bitrix\Rest\RestException;
use Bitrix\Main\Mail\Event;
use Bitrix\Main\Localization\Loc;

//require_once($_SERVER['DOCUMENT_ROOT'].'/PHPExcel-1.8.1/Classes/PHPExcel.php');

Loc::loadMessages(__FILE__);

/**
 * This class used for internal use only, not a part of public API.
 * It can be changed at any time without notification.
 *
 * @access private
 */

final class Provinsi { 
    const TABLE_MASTER = 'b_as_provinsi';

    public static function format_for_db($request,$table=''){
        global $DB;
        $arRequest = array();
        if($table != ''){
            $field = $DB->query("SHOW columns FROM ".$table);
            $arFields = array();
            while($row = $field->fetch()){
                $arFields[] = $row['Field'];
            }

            foreach($request as $key => $value){
                if(in_array($key,$arFields)){
                    if($request[$key] == 'NULL'){
                        $arRequest[$key] = "NULL";
                    }
                    else{
                        $arRequest[$key] = "'".$request[$key]."'";
                    }
                }
            }
        }
        else{
            foreach($request as $key => $value){
                if($request[$key] == 'NULL'){
                    $arRequest[$key] = "NULL";
                }
                else{
                    $arRequest[$key] = "'".$request[$key]."'";
                }
            }
        }
        return $arRequest;
    }
    
    public static function postToPortalVendor($url_api,$params){ 
        $host_portal_vendor = ProcurementModule::setting_get('vendor_portal_url');
        $host_portal_vendor = $host_portal_vendor['VALUE']; 

        $curl = curl_init(); 
        $set_url = $host_portal_vendor.$url_api; 
        curl_setopt_array($curl, array(
            CURLOPT_URL => $set_url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            // CURLOPT_HTTPHEADER => array(
            //     'Authorization: Bearer ' . $token
            // ),
            CURLOPT_POSTFIELDS => $params,
        ));

        $response = curl_exec($curl); 
        $res = json_decode($response);

        if(empty($res)){
            return array(
                'status'=>'error',
                'data'=>$response,
            );
        } 

        curl_close($curl);
        return array(
            'status' => 'success',
            'data' => $res,
        ); 
    } 

    public static function GetByID($id = ""){
        global $USER, $DB;
        $user_id = $USER->GetID(); // id user login
        $data_user = CUser::GetByID($user_id)->Fetch();
        $date = new DateTime();
        $current_date = $date->format('Y-m-d H:i:s');

        $id = $DB->ForSql($id); 

        $sql = "
            SELECT 
                *
            FROM 
                " . self::TABLE_MASTER . "
            ";
        if ($id != ""){
            $sql .= " WHERE ID='".$id."' ";
        }
        $sql .= " ORDER BY NAMA_PROVINSI ";
        $query = $DB->Query($sql);
        $count = $query->SelectedRowsCount();
        if($count == 0){
            return array(
                'result'=>false 
            );
        }
        else if ($count == 1){
            $res = $query->Fetch(); // jika hanya ingin mengambil 1 data
        }
        else {
         // jika hanya ingin mengambil lebih dari 1 data
            $res = [];
            while($row = $query->Fetch()){
                $res[] = $row;
            }
        }
        return $res;
    }

    public static function save($arData){
        global $USER, $DB; 
        $user_id = $USER->GetID();
        $date = new DateTime();
        $current_date = $date->format('Y-m-d H:i:s'); 

        $DB->StartTransaction(); 
        $fields = array( 
            'NAMA_PROVINSI' => $arData['NAMA_PROVINSI'],
            'CREATED_BY' => $user_id,
            'CREATED_DATE' => $current_date,
        );  

        $res_id = $DB->Insert(
            self::TABLE_MASTER,
            self::format_for_db($fields), 
            "", false, "", $ignore_errors=true
        );

        if(empty($res_id)){
            $DB->Rollback();  
            return false;
        }
        
        $DB->Commit(); 
        return $res_id; 
    } 

    public static function update($arData){
        global $USER, $DB; 
        $user_id = $USER->GetID();
        $date = new DateTime();
        $current_date = $date->format('Y-m-d H:i:s');
        
        $id = $DB->ForSql($arData['ID']); 
        
        $sql = "
            SELECT 
                *
            FROM 
                " . self::TABLE_MASTER . "  
            WHERE 
                ID = '".$id."' 
        ";
        $query = $DB->Query($sql);
        $count = $query->SelectedRowsCount();
        if($count == 0){
            return false;
        }
        $data_master = $query->Fetch(); 

        $DB->StartTransaction(); 
        $fields = array( 
            'NAMA_PROVINSI' => $arData['NAMA_PROVINSI'],
            'MODIFIED_BY' => $user_id,
            'MODIFIED_DATE' => $current_date,
        ); 

        $res_id = $DB->Update(
            self::TABLE_MASTER,
            // CAskarasoft::format_for_db($fields), 
            self::format_for_db($fields), 
            "WHERE ID='".$id."'", 
            "", false, "", $ignore_errors=true
        ); 

        if(empty($res_id)){
            $DB->Rollback(); 
            return false;
        }  

        $DB->Commit();
        return true;
    } 
}