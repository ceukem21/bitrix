<?
global $DOCUMENT_ROOT, $MESS;

IncludeModuleLangFile(__FILE__);

Class askarasoft_provinsi extends CModule
{
	var $MODULE_ID = "askarasoft.provinsi";
	var $MODULE_VERSION;
	var $MODULE_VERSION_DATE;
	var $MODULE_NAME;
	var $MODULE_DESCRIPTION;
	var $MODULE_CSS;
	var $MODULE_GROUP_RIGHTS = "Y";

	function __construct()
	{
		$arModuleVersion = [];

		$path = str_replace("\\", "/", __FILE__);
		$path = substr($path, 0, strlen($path) - strlen("/index.php"));
		include($path."/version.php");

		if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion))
		{
			$this->MODULE_VERSION = $arModuleVersion["VERSION"];
			$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
		}

		$this->MODULE_NAME = "Provinsi";
		$this->MODULE_DESCRIPTION = "module for Provinsi Indonesia";
	}

	function InstallEvents()
    {
        CModule::IncludeModule("{$this->MODULE_ID}");
        $eventManager = \Bitrix\Main\EventManager::getInstance();
        return true;
    }
    
    /**
     * UnInstallEvents
     * @return boolean
     */
    function UnInstallEvents()
    {
        $eventManager = \Bitrix\Main\EventManager::getInstance();
        return true;
    }   
    
    /**
     * Óñòàíîâêà ôàéëîâ
     * @return boolean
     */
    function InstallFiles()
    {
		return true;
    }
    
    /**
     * Óäàëåíèå ôàéëîâ
     * @return boolean
     */        
    function UnInstallFiles()
    {
        return true;
    }    
    
    /**
     * InstallDB() Èíñòàëëÿòîð òàáëèö â ÁÄ
     * @access public
     * @return boolean
     */
    function InstallDB()
    {
        global $DB,$APPLICATION;
        $this->errors = false;
        $this->errors = $DB->RunSQLBatch($_SERVER["DOCUMENT_ROOT"]."/local/modules/{$this->MODULE_ID}/install/db/mysql/install.sql");

        

	   return true;
    }   
    
    /**
     * UnInstallDB() Äåèíñòàëëÿòîð òàáëèö
     * @access public
     * @return boolean
     */
    function UnInstallDB()
    {
        $this->errors = false;
        global $DB;
        $this->errors = $DB->RunSQLBatch($_SERVER["DOCUMENT_ROOT"]."/local/modules/{$this->MODULE_ID}/install/db/mysql/uninstall.sql");

        //CAgent::RemoveModuleAgents("askarasoft.icp");
		return true;
    }     

    function InstallUF() {
        
    }


	function DoInstall()
	{
		global $APPLICATION;

		if (!IsModuleInstalled("{$this->MODULE_ID}")){

            RegisterModule("{$this->MODULE_ID}");
            CModule::IncludeModule("{$this->MODULE_ID}");
            $this->InstallEvents();
            $this->InstallDB();
		}
	}

	function DoUninstall()
	{
		global $DB, $APPLICATION, $USER, $step;
        if (IsModuleInstalled("{$this->MODULE_ID}")){
            $this->UnInstallEvents();
            $this->UnInstallDB();
            UnRegisterModule("{$this->MODULE_ID}");
        }
	}
}
?>