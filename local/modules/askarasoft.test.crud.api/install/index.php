<?
global $DOCUMENT_ROOT, $MESS;

IncludeModuleLangFile(__FILE__);

Class askarasoft_test_crud_api extends CModule
{
    var $MODULE_ID = "askarasoft.test.crud.api"; //modul id ini nanti otomatis tersimpan di database (primary)
    var $MODULE_VERSION = "1.0.0"; //modul versi fungsinya untuk identitas saja (disarankan untuk diisi)
    var $MODULE_VERSION_DATE = "2020-10-30 13:53:00"; //ini juga disarankan untuk diisi
    var $MODULE_NAME = "Askarasoft Test CRUD API"; //nama modul wajib diisi
    var $MODULE_DESCRIPTION = "api for REST";
    var $PARTNER_NAME = "(Askarasoft) Muhammad Aprian Fauzi";
    var $PARTNER_URI = "http://www.askarasoft.com/";
    var $MODULE_CSS;
    var $MODULE_GROUP_RIGHTS = "Y";

    function __construct()
    {
        $arModuleVersion = [];

        $path = str_replace("\\", "/", __FILE__);
        $path = substr($path, 0, strlen($path) - strlen("/index.php"));
    }

	function InstallEvents()
    {
        
        return true;
    }
    
    /**
     * UnInstallEvents
     * @return boolean
     */
    function UnInstallEvents()
    {
        return true;
    }    
    
    /**
     * Óñòàíîâêà ôàéëîâ
     * @return boolean
     */
    function InstallFiles()
    {
		return true;
    }
    
    /**
     * Óäàëåíèå ôàéëîâ
     * @return boolean
     */        
    function UnInstallFiles()
    {
        return true;
    }    
    
    /**
     * InstallDB() Èíñòàëëÿòîð òàáëèö â ÁÄ
     * @access public
     * @return boolean
     */
    function InstallDB()
    {
        global $DB,$APPLICATION;
        //please install db for work order csv
        //setup db for custom field work order
        $this->errors = false;
        
        if($this->errors !== false)
	    {
            $APPLICATION->ThrowException(implode("<br>", $this->errors));
            return false;
    	}
        else
        {
            RegisterModule("{$this->MODULE_ID}");
            CModule::IncludeModule("{$this->MODULE_ID}");
			$eventManager = \Bitrix\Main\EventManager::getInstance();
			$eventManager->registerEventHandlerCompatible('rest', 'OnRestServiceBuildDescription', "{$this->MODULE_ID}", 'TestCrudAPI', 'OnRestServiceBuildDescription');
			// $eventManager->registerEventHandlerCompatible('rest', 'OnRestServiceBuildDescription', 'askarasoft.api', 'PurchaseRequisitionAPI', 'OnRestServiceBuildDescription');
        }

        $DB->RunSQLBatch($_SERVER["DOCUMENT_ROOT"]."/local/modules/".$this->MODULE_ID."/install/db/mysql/install.sql");

	return true;
    }   
    
    /**
     * UnInstallDB() Äåèíñòàëëÿòîð òàáëèö
     * @access public
     * @return boolean
     */
    function UnInstallDB()
    {
        $this->errors = false;
        global $DB;
        //$this->errors = $DB->RunSQLBatch($_SERVER["DOCUMENT_ROOT"]."/local/modules/askarasoft.api/install/db/mysql/uninstall.sql");

        //CAgent::RemoveModuleAgents("askarasoft.mobile");
        $eventManager = \Bitrix\Main\EventManager::getInstance();
		$eventManager->unRegisterEventHandler('rest', 'OnRestServiceBuildDescription', "{$this->MODULE_ID}", 'TestCrudAPI', 'OnRestServiceBuildDescription');
		// $eventManager->unRegisterEventHandler('rest', 'OnRestServiceBuildDescription', 'askarasoft.api', 'PurchaseRequisitionAPI', 'OnRestServiceBuildDescription');
        
		UnRegisterModule("{$this->MODULE_ID}");
		return true;
    }     

    function InstallUF() {
    
    }


	function DoInstall()
	{
		global $APPLICATION;

		if (!IsModuleInstalled("{$this->MODULE_ID}"))
		{
			$this->InstallFiles();
			$this->InstallDB();
			$this->InstallUF();
			$this->InstallEvents();

			$GLOBALS["errors"] = $this->errors;

			$APPLICATION->IncludeAdminFile("Install", $_SERVER["DOCUMENT_ROOT"]."/local/modules/".$this->MODULE_ID."/install/step.php");
		}
	}

	function DoUninstall()
	{
		global $DB, $APPLICATION, $USER, $step;
		if($USER->IsAdmin())
		{
				$this->UnInstallDB();
				$this->UnInstallEvents();
				$this->UnInstallFiles();

				$GLOBALS["errors"] = $this->errors;
				$APPLICATION->IncludeAdminFile(GetMessage("CAL_UNINSTALL_TITLE"), $_SERVER["DOCUMENT_ROOT"]."/local/modules/".$this->MODULE_ID."/install/unstep.php");
		}
	}
}
?>