<?php
// \Bitrix\Main\Loader::includeModule('askarasoft.module.v2');
// \Bitrix\Main\Loader::includeModule('askarasoft.pr');
// \Bitrix\Main\Loader::includeModule('askarasoft.spph');  
// \Bitrix\Main\Loader::includeModule('askarasoft.customfield');
use Bitrix\Rest\RestException;

final class TestDbAPI extends IRestService
{
    const SCOPE_NAME = 'database';
    const TABLE_MASTER = 'b_as_test_crud';

    public static function format_for_db($request,$table=''){
        global $DB;
        $arRequest = array();
        if($table != ''){
            $field = $DB->query("SHOW columns FROM ".$table);
            $arFields = array();
            while($row = $field->fetch()){
                $arFields[] = $row['Field'];
            }

            foreach($request as $key => $value){
                if(in_array($key,$arFields)){
                    if($request[$key] == 'NULL'){
                        $arRequest[$key] = "NULL";
                    }
                    else{
                        $arRequest[$key] = "'".$request[$key]."'";
                    }
                }
            }
        }
        else{
            foreach($request as $key => $value){
                if($request[$key] == 'NULL'){
                    $arRequest[$key] = "NULL";
                }
                else{
                    $arRequest[$key] = "'".$request[$key]."'";
                }
            }
        }
        return $arRequest;
    }

    public static function OnRestServiceBuildDescription()
    {
        $methods = [  
            "db.get_db_crud" => [__CLASS__, "getTestCrud"],  
            "db.post_db_crud" => [__CLASS__, "save"],  
            "db.patch_db_crud" => [__CLASS__, "update"],  
            "db.delete_db_crud" => [__CLASS__, "setPatchCrud"],  
        ];

        return [self::SCOPE_NAME => $methods];
    }  
    
    // PROCESS_TYPE
    // CONTENT
    // POST_DATE

    // jalankan di php command line :
    // $eventManager = \Bitrix\Main\EventManager::getInstance();
    // $eventManager->registerEventHandlerCompatible('rest', 'OnRestServiceBuildDescription', 'askarasoft.test.crud.api', 'TestCrudAPI', 'OnRestServiceBuildDescription'); 

    public static function getTestCrud($data = []){
        if(strtoupper($_SERVER['REQUEST_METHOD']) != 'POST'){
            return array(
                'status' => 'error',
                'message' => 'Request Method must be POST'
            );
        }
            global $USER, $DB;
            $user_id = $USER->GetID(); // id user login
            $data_user = CUser::GetByID($user_id)->Fetch();
            $date = new DateTime();
            $current_date = $date->format('Y-m-d H:i:s');
    
            $id = $DB->ForSql($data['ID']); 
            $sql = "
                SELECT 
                    *
                FROM 
                    " . self::TABLE_MASTER . "   
                ";
            if (!empty($id)){
                $sql .= "WHERE ID='".$id."'";
            }

            $sql .= " ORDER BY ID ASC ";
            $query = $DB->Query($sql);
            $count = $query->SelectedRowsCount();
            if($count == 0){
                return array(
                    'result'=>false 
                );
            }
    
            // jika hanya ingin mengambil lebih dari 1 data
            // $res = [];
            while($row = $query->Fetch()){
                $res[] = $row;
            }
    
            // $res = $query->Fetch(); // jika hanya ingin mengambil 1 data
        
          
        // return array(
        //     'status' => 'error',
        //     'message' => 'test'
        // ); 

        return array(
            'status' => 'success',
            'data' => $res,
        );
    }
    
    public static function save($data = []){
        if(strtoupper($_SERVER['REQUEST_METHOD']) != 'POST'){
            return array(
                'status' => 'error',
                'message' => 'Request Method must be POST'
            );
        }
        global $USER, $DB; 
        $user_id = $USER->GetID();
        $date = new DateTime();
        $current_date = $date->format('Y-m-d H:i:s'); 

        $DB->StartTransaction(); 
        $fields = array( 
            'NAMA' => $data['NAMA'],
            'ALAMAT' => $data['ALAMAT'],
            'TELEPON' => $data['TELEPON'],
            'STATUS' => 'NEW',
            'CREATED_BY' => $user_id,
            // 'CREATED_DATE' => $current_date,
        );  

        $res_id = $DB->Insert(
            self::TABLE_MASTER,
            self::format_for_db($fields), 
            "", false, "", $ignore_errors=true
        );

        if(empty($res_id)){
            $DB->Rollback();  
            return false;
        }
        
        $DB->Commit();
        $sql = "
                SELECT 
                    ID, 
                    (SELECT count(*) FROM " . self::TABLE_MASTER ." LIMIT 1) as JUM
                FROM 
                    " . self::TABLE_MASTER . "    
                ORDER BY ID DESC
                LIMIT 1
                ";
        $query = $DB->Query($sql); 
        while($row = $query->Fetch()){
            $res[] = $row;
        }
        return array(
            'status' => 'success',
            'data' => $res,
        ); 
    }
    
    public static function update($data){
        if(strtoupper($_SERVER['REQUEST_METHOD']) != 'POST'){
            return array(
                'status' => 'error',
                'message' => 'Request Method must be POST'
            );
        }
        global $USER, $DB; 
        $user_id = $USER->GetID();
        $date = new DateTime();
        $current_date = $date->format('Y-m-d H:i:s');
        
        $id = $DB->ForSql($data['ID']); 
        
        $sql = "
            SELECT 
                *
            FROM 
                " . self::TABLE_MASTER . "  
            WHERE 
                ID = '".$id."' 
        ";
        $query = $DB->Query($sql);
        $count = $query->SelectedRowsCount();
        if($count == 0){
            return false;
        }
        $data_master = $query->Fetch(); 

        $DB->StartTransaction(); 
        $fields = array( 
            'NAMA' => $data['NAMA'],
            'ALAMAT' => $data['ALAMAT'],
            'TELEPON' => $data['TELEPON'],
            'STATUS' => 'draft',
            'MODIFIED_BY' => $user_id,
            'MODIFIED_DATE' => $current_date,
        ); 

        $res_id = $DB->Update(
            self::TABLE_MASTER,
            // CAskarasoft::format_for_db($fields), 
            self::format_for_db($fields), 
            "WHERE ID='".$id."'", 
            "", false, "", $ignore_errors=true
        ); 

        if(empty($res_id)){
            $DB->Rollback();
            return array(
                'status' => 'error',
                'message' => 'test'
            );
        }  

        $DB->Commit();
        return array(
            'status' => 'success',
            'data' => $res_id,
        );
    }
    
}