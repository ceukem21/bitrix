<?php
// \Bitrix\Main\Loader::includeModule('askarasoft.module.v2');
// \Bitrix\Main\Loader::includeModule('askarasoft.pr');
// \Bitrix\Main\Loader::includeModule('askarasoft.spph');  
// \Bitrix\Main\Loader::includeModule('askarasoft.customfield');
use Bitrix\Rest\RestException;

final class TestCrudAPI extends IRestService
{
    const SCOPE_NAME = 'eprocurement';

    public static function OnRestServiceBuildDescription()
    {
        $methods = [  
            "test.get_test_crud" => [__CLASS__, "getTestCrud"],  
        ];

        return [self::SCOPE_NAME => $methods];
    }  
    
    // PROCESS_TYPE
    // CONTENT
    // POST_DATE

    // jalankan di php command line :
    // $eventManager = \Bitrix\Main\EventManager::getInstance();
    // $eventManager->registerEventHandlerCompatible('rest', 'OnRestServiceBuildDescription', 'askarasoft.test.crud.api', 'TestCrudAPI', 'OnRestServiceBuildDescription'); 

    public static function getTestCrud($data = []){
        if(strtoupper($_SERVER['REQUEST_METHOD']) != 'POST'){
            return array(
                'status' => 'error',
                'message' => 'Request Method must be POST'
            );
        }
        global $USER, $DB; 
        
        // if(empty($data)){ 
        //     return array(
        //         'status' => 'error',
        //         'message' => 'Params not found'
        //     );
        // }

        $arIBlock = CIBlock::GetList([], ['CODE' => 'PROC_TEST_CRUD'])->Fetch(); 

        $arFilter=array(
            'IBLOCK_ID' => $arIBlock['ID'],   
        );

        if(!empty($data['ID'])){
            $arFilter['ID'] = $data['ID'];
        }
        if(!empty($data['CODE'])){
            $arFilter['NAME'] = '%'.$data['CODE'].'%';
        }
        if(!empty($data['JABATAN'])){
            $arFilter['PROPERTY_JABATAN_VALUE'] = $data['JABATAN'];
        }
        if(!empty($data['TANGGAL'])){
            if(strlen($data['TANGGAL']) > 10){
                $arFilter['PROPERTY_TANGGAL'] = '%'.date("Y-m-d H:i", strtotime($data['TANGGAL'])).'%'; 
            }else{
                $arFilter['PROPERTY_TANGGAL'] = '%'.date("Y-m-d", strtotime($data['TANGGAL'])).'%'; 
            }
        } 

        // // return $arFilter;
        
        // $arLimit = []; 
        // $show_from = '';
        // $show_to = '';
        // $str_limit = '';

        // if(!empty($data['TOTAL_SHOW'])){
        //     $str_limit = $data['TOTAL_SHOW']; 
        // } else { 
        //     if(isset($data['SHOW_FROM']) && isset($data['SHOW_TO'])){
        //         $show_from = $data['SHOW_FROM'];
        //         $show_to = $data['SHOW_TO'];
        //     } 

        //     if($show_from != '' && $show_to != ''){
        //         $str_limit = "$show_from, $show_to";
        //     } 
        // }
        
        // if(isset($str_limit) && $str_limit != ''){
        //     $arLimit = ["nTopCount"=>$str_limit];  
        // } 

        $get_list = CIBlockElement::GetList(
            [
                // 'PROPERTY_TANGGAL_POSTING' => 'DESC',
                'ID' => 'DESC',
            ],  
            $arFilter, 
            false, 
            $arLimit, 
            [
                'ID',
                'NAME',
                'PROPERTY_JABATAN',
                'PROPERTY_TANGGAL', 
            ]
        );

        // return $get_list;
        $urlManager = \Bitrix\Main\Engine\UrlManager::getInstance();
        $host = $urlManager->getHostUrl(); 

        $res = [];
        while ($r = $get_list->GetNextElement()) {  
            $r2 = $r->getFields(); 

            $r3 = [];
            foreach($r2 as $key => $val){ 
                if (strpos($key, '_VALUE_ID') !== false) {
                    continue;
                }

                if (strpos($key, '~') === false) {
                    $key = str_replace('PROPERTY_','',$key);
                    $key = str_replace('_ENUM_','_',$key);
                    // $key = str_replace('_VALUE','',$key);
                    if(substr($key,-6) == '_VALUE'){
                        $key = substr($key,0,-6);
                    }
                    
                    if(is_array($val)){
                        if(isset($val['TYPE']) && $val['TYPE'] == 'HTML'){
                            $r3[$key] = $val['TEXT'];
                        }else{ 
                            $r3[$key] = $val;
                        }
                    }else{
                        $r3[$key] = $val;
                    }
                } 
            }
            $res[] = $r3;
            // $res[] = $r2;
        }
          
        // return array(
        //     'status' => 'error',
        //     'message' => 'test'
        // ); 

        return array(
            'status' => 'success',
            'data' => $res,
        );
    }  
    
}