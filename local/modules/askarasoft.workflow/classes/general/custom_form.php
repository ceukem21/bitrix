<?
class CCustomForm
{
    protected static $Component = false;
    const IBLOCK_TYPE_ID = 'bitrix_processes';

    /*
     *
     * */
    private static function GetFlowIBlock($workflowName)
    {
        $arIBlock = CIBlock::GetList([],['NAME' => $workflowName])->Fetch();
        if ($arIBlock) {
            $obList = new CList($arIBlock['ID']);
            $listFields = $obList->GetFields();
            $arIBlockData = [
                'IBLOCK_ID'  => $arIBlock['ID'],
                'FIELDS'     => $listFields,
            ];
        }
        return $arIBlockData;
    }

    /*
     * dipakai untuk mempersiapkan value sebelum di insert ke b_iblock_element
     */
    private static function prepareFlowData($arFields, $arData)
    {
        $arReturn = [];
        $arReturn['PROPERTY_VALUES'] = [];

        foreach($arFields as $fieldID => $arField) {
            if (!$arField['PROPERTY_TYPE']) {
                if (isset($arData[$fieldID]))
                    $arReturn[$fieldID] = $arData[$fieldID];
                else
                    $arReturn[$fieldID] = $arField['DEFAULT_VALUE'];
            } else {
                $key = $arField['CODE'];
                if (!isset($arData[$key])) {
                    $arReturn['PROPERTY_VALUES'][$arField['ID']] = $arField['DEFAULT_VALUE'];
                    continue;
                }
                switch($arField['PROPERTY_TYPE'])  {
                    case 'F': // file
                        $arReturn["PROPERTY_VALUES"][$arField['ID']] = array();
                        if(!empty($arData[$key])) {
                            \CFile::ConvertFilesToPost($arData[$key], $arReturn["PROPERTY_VALUES"][$arField['ID']]);
                        }
                        break;
                    case 'L': //list
                        if (is_array($arData[$key])) {
                            foreach($arData[$key] as $i => $j) {
                                $res = CIBlockPropertyEnum::GetList([],['PROPERTY_ID' => $arField['ID'], 'VALUE' => $j]);
                                if ($res && ($ar = $res->Fetch())) {
                                    $arData[$key][$i] = $ar['ID'];
                                }
                            }
                            $arReturn["PROPERTY_VALUES"][$arField['ID']] = $arData[$key];
                        } else {
                            $arF = ['PROPERTY_ID' => $arField['ID'], 'VALUE' => $arData[$key]];
                            $res = CIBlockPropertyEnum::GetList([], $arF);
                            if ($res && $ar = $res->Fetch()) {
                                $arData[$key] = $ar['ID'];
                            }
                            $arReturn["PROPERTY_VALUES"][$arField['ID']]["VALUE"] = $arData[$key];
                        }
                        break;
                    case "S": //string , date , html/text
                        if ($arField['PROPERTY_USER_TYPE']['USER_TYPE'] == 'HTML'){
                            if (is_array($arData[$key])) {
                                foreach ($arData[$key] as $i => $j) {
                                    $newData[$i]['VALUE'] = ['TYPE' => 'HTML', 'TEXT'=>$j];
                                }
                            } else {
                                $newData['VALUE'] = ['TYPE' => 'HTML', 'TEXT' => $arData[$key]];
                            }
                            $arReturn["PROPERTY_VALUES"][$arField['ID']] = $newData;
                        } else {
                            if (is_array($arData[$key])) {
                                foreach($arData[$key] as $i => $j) {
                                    $newData[$i]['VALUE'] = $j;
                                }
                                $arData[$key] = $newData;
                            } else {
                                $arData[$key]['VALUE'] = $arData[$key];
                            }
                            $arReturn["PROPERTY_VALUES"][$arField['ID']] = $arData[$key];
                        }
                        break;
                    case 'N':
                        if (is_array($arData[$key])) {
                            foreach ($arData[$key] as $i => $j) {
                                $arData[$key][$i] = floatval($j);
                            }
                        } else {
                            $arData[$key] = floatval($arData[$key]);
                        }
                        $arReturn["PROPERTY_VALUES"][$arField['ID']] = $arData[$key];
                        break;
                    default :
                        if (is_array($arData[$key])) {

                            $arReturn["PROPERTY_VALUES"][$arField['ID']] = $arData[$key];
                        }else
                            $arReturn["PROPERTY_VALUES"][$arField['ID']]["VALUE"] = $arData[$key];
                        break;
                }
            }
        }

        return $arReturn;
    }

    public static function AddToWorkflow($arData)
    {
        $arResult = [];

        if (isset($arData['ELEMENT_ID']))
            $arResult['ELEMENT_ID'] = $arData['ELEMENT_ID'];

        $arIBlockData = self::GetFlowIBlock($arData['WORKFLOW_NAME']);

        $arFields  = self::prepareFlowData($arIBlockData['FIELDS'], $arData);
        $arFields['IBLOCK_ID'] = $arIBlockData['IBLOCK_ID'];

        $documentType = BizProcDocument::generateDocumentComplexType(self::IBLOCK_TYPE_ID, $arFields['IBLOCK_ID']);
        $arDocumentStates = CBPDocument::GetDocumentStates(
            $documentType,
            ($arResult["ELEMENT_ID"] > 0) ? BizProcDocument::getDocumentComplexId(
                self::IBLOCK_TYPE_ID, $arResult["ELEMENT_ID"]) : null,
            "Y"
        );

        $obElement = new CIBlockElement();
        if ($arResult['ELEMENT_ID'] > 0)
            $res = $obElement->Update($arResult['ELEMENT_ID'], $arFields, false, true, true);
        else {
//            echo "<pre>";
//            die(var_dump($arFields));
            $res = $obElement->Add($arFields, true, true);
            if ($res) {
                $arResult["ELEMENT_ID"] = $res;
            } else
                $strError = $obElement->LAST_ERROR;
        }

        if(!$strError) {

            foreach($arDocumentStates as $arDocumentState)
            {
                if(strlen($arDocumentState["ID"]) <= 0)
                {
                    $arErrorsTmp = array();
                    $parameters = [];
                    $arBizProcWorkflowId[$arDocumentState["TEMPLATE_ID"]] = CBPDocument::StartWorkflow(
                        $arDocumentState["TEMPLATE_ID"],
                        BizProcDocument::getDocumentComplexId(self::IBLOCK_TYPE_ID, $arResult["ELEMENT_ID"]),
                        $parameters,
                        $arErrorsTmp
                    );

                    foreach($arErrorsTmp as $e)
                        $strError .= $e["message"]."<br />";
                }
            }
            if ($strError) {
                ShowError($strError);
                return false;
            }
        }else{
            ShowError($strError);
            return false;
        }

        return $arResult;
    }
}
