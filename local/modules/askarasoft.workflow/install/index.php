<?
global $DOCUMENT_ROOT, $MESS;

IncludeModuleLangFile(__FILE__);

Class askarasoft_workflow extends CModule
{
	var $MODULE_ID = "askarasoft.workflow";
	var $MODULE_VERSION;
	var $MODULE_VERSION_DATE;
	var $MODULE_NAME;
	var $MODULE_DESCRIPTION;
	var $MODULE_CSS;
	var $MODULE_GROUP_RIGHTS = "Y";

	function __construct()
	{
		$arModuleVersion = array();

		$path = str_replace("\\", "/", __FILE__);
		$path = substr($path, 0, strlen($path) - strlen("/index.php"));
		include($path."/version.php");

		if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion))
		{
			$this->MODULE_VERSION = $arModuleVersion["VERSION"];
			$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
		}

		$this->MODULE_NAME = "Custom Form to Workflow";
		$this->MODULE_DESCRIPTION = "Module untuk Link Custom Form to Workflow";
	}

	function InstallEvents()
    {
        return true;
    }
    
    /**
     * UnInstallEvents
     * @return boolean
     */
    function UnInstallEvents()
    {
        return true;
    }    
    
    /**
     * Óñòàíîâêà ôàéëîâ
     * @return boolean
     */
    function InstallFiles()
    {
		return true;
    }
    
    /**
     * Óäàëåíèå ôàéëîâ
     * @return boolean
     */        
    function UnInstallFiles()
    {
        return true;
    }    
    
    /**
     * InstallDB() Èíñòàëëÿòîð òàáëèö â ÁÄ
     * @access public
     * @return boolean
     */
    function InstallDB()
    {
        global $DB;
        $this->errors = false;

        $this->errors = $DB->RunSQLBatch($_SERVER["DOCUMENT_ROOT"]."/local/modules/{$this->MODULE_ID}/install/db/mysql/install.sql");

        if($this->errors !== false)
        {
                // Âûäàþ èñêëþ÷åíèå (ìîæåò áûòü ôàòàëüíûé ðåçóëüòàò)
                $this->APPLICATION->ThrowException(implode("<br>", $this->errors));
        }
        else
        {
            RegisterModule($this->MODULE_ID);
            CModule::IncludeModule($this->MODULE_ID);
            $siteId = \CSite::GetDefSite();
        }
	return true;
    }   
    
    /**
     * UnInstallDB() Äåèíñòàëëÿòîð òàáëèö
     * @access public
     * @return boolean
     */
    function UnInstallDB()
    {
        global $DB;
        $this->errors = $DB->RunSQLBatch($_SERVER["DOCUMENT_ROOT"]."/local/modules/{$this->MODULE_ID}/install/db/mysql/uninstall.sql");
		$eventManager = \Bitrix\Main\EventManager::getInstance();
        UnRegisterModule($this->MODULE_ID);
		return true;
    }     
	
	function DoInstall()
	{
		global $APPLICATION;

		if (!IsModuleInstalled($this->MODULE_ID))
		{
			$this->InstallFiles();
			$this->InstallDB();
			$this->InstallEvents();

			$GLOBALS["errors"] = $this->errors;

			$APPLICATION->IncludeAdminFile("Install", $_SERVER["DOCUMENT_ROOT"]."/local/modules/{$this->MODULE_ID}/install/step.php");
		}
	}

	function DoUninstall()
	{
		global $DB, $APPLICATION, $USER, $step;
		if($USER->IsAdmin())
		{
				$this->UnInstallDB();
				$this->UnInstallEvents();
				$this->UnInstallFiles();

				$GLOBALS["errors"] = $this->errors;
				$APPLICATION->IncludeAdminFile(GetMessage("CAL_UNINSTALL_TITLE"), $_SERVER["DOCUMENT_ROOT"]."/local/modules/{$this->MODULE_ID}/install/unstep.php");
		}
	}
}
?>