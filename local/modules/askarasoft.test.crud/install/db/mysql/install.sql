DROP TABLE IF EXISTS `b_as_test_crud`; 

CREATE TABLE `b_as_test_crud` (
  `ID` INT NOT NULL AUTO_INCREMENT, 
  `NAMA` VARCHAR(150) NOT NULL, 
  `ALAMAT` TEXT NULL, 
  `TELEPON` VARCHAR(50) NOT NULL, 
  `STATUS` VARCHAR(100) NOT NULL, 
  `CREATED_BY` INT NOT NULL, 
  `CREATED_DATE` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, 
  `MODIFIED_BY` INT NULL, 
  `MODIFIED_DATE` DATETIME NULL, 
  PRIMARY KEY (`ID`)
); 